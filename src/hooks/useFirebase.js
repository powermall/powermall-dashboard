import axios from "axios";
import { createUserWithEmailAndPassword, getAuth, onAuthStateChanged, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import initApp from "../firebase/Firebase.init";

initApp();

const useFirebase = () => {

    const [user, setUser] = useState({});
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(true);
    const auth = getAuth();

    useEffect(() => {

        const subscribed = onAuthStateChanged(auth, (user) => {

            if (user) {
                setUser(user);
            } else {
                setUser({});
            }
            setLoading(false);
        });

        return () => subscribed;

    }, [auth]);

    const signUpWithEmailPassword = (email, password, role) => {

        createUserWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                const user = userCredential.user;
                setUser(user);

                axios.post('https://api.powermall.com.bd/roles', { email, password, role })
                    .then(res => {
                        if (res.status === 200) {
                            Swal.fire('Success', 'Account Created Successfully', 'success');
                        }
                    });

            })
            .catch((error) => {
                const errorMessage = error.message;
                setError(errorMessage);
            })
            .finally(() => setLoading(false));

    };

    const signInWithEmailPassword = (email, password, navigate) => {
        signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                const user = userCredential.user;
                setUser(user);
                navigate('/dashboard/sales');
            })
            .catch((error) => {
                const errorMessage = error.message;
                setError(errorMessage);
            })
            .finally(() => setLoading(false));
        axios.get(`https://api.powermall.com.bd/roles/${email}`)
            .then(res => {

                if (res.data.email === email) {

                    signInWithEmailAndPassword(auth, email, password)
                        .then((userCredential) => {
                            const user = userCredential.user;
                            setUser(user);
                            navigate('/dashboard/sales');
                        })
                        .catch((error) => {
                            const errorMessage = error.message;
                            setError(errorMessage);
                        })
                        .finally(() => setLoading(false));

                } else (
                    Swal.fire('Error 401', "Access Denied (You Don't Have Permission)", 'error')
                );

            });

    };

    const SignOut = (navigate) => {

        setLoading(true);

        signOut(auth).then(() => {
            setUser({});
            navigate('/login');
        }).catch((error) => {
            setError(error.message);
        })
            .finally(() => setLoading(false));

    };

    return {
        user,
        signUpWithEmailPassword,
        signInWithEmailPassword,
        SignOut,
        loading,
        error,
        setError,
    };
};

export default useFirebase;