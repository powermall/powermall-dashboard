import { BrowserRouter, Route, Routes } from "react-router-dom";
import AdminLogin from "./components/admin/AdminLogin";
import AdminRoute from "./components/admin/AdminRoute";
import CreateAccount from "./components/admin/CreateAccount";
import IndexDashboard from "./components/dashboard/IndexDashboard";
import AddFeatures from "./components/dashboard/pages/AddFeature";
import AddBrands from "./components/dashboard/pages/additional-pages/AddBrands";
import AddCategory from "./components/dashboard/pages/additional-pages/AddCategory";
import AddCoupons from "./components/dashboard/pages/additional-pages/AddCoupons";
import AddProducts from "./components/dashboard/pages/additional-pages/AddProducts";
import InvoicePage from "./components/dashboard/pages/additional-pages/InvoicePage";
import UpdateCategory from "./components/dashboard/pages/additional-pages/UpdateCategory";
import UpdateCoupons from "./components/dashboard/pages/additional-pages/UpdateCoupons";
import UpdateFeatures from "./components/dashboard/pages/additional-pages/UpdateFeature";
import UpdateProducts from "./components/dashboard/pages/additional-pages/UpdateProducts";
import Brands from "./components/dashboard/pages/Brands";
import Category from "./components/dashboard/pages/Category";
import Contacts from "./components/dashboard/pages/Contacts";
import Coupons from "./components/dashboard/pages/Coupons";
import Features from "./components/dashboard/pages/Features";
import FooterPages from "./components/dashboard/pages/FooterPages";
import Orders from "./components/dashboard/pages/Orders";
import OrderStatus from "./components/dashboard/pages/OrderStatus";
import Products from "./components/dashboard/pages/Products";
import Refunds from "./components/dashboard/pages/Refunds";
import Sales from "./components/dashboard/pages/Sales";
import Settings from "./components/dashboard/pages/Settings";
import Shippings from "./components/dashboard/pages/Shippings";
import Transection from "./components/dashboard/pages/Transection";
import Users from "./components/dashboard/pages/Users";
import Wishist from "./components/dashboard/pages/Wishist";
import Withdraws from "./components/dashboard/pages/Withdraws";
import AuthProvider from "./contexts/AuthProvider";

function App() {

	const paths = [
		{ to: '*', element: <Sales /> },
		{ to: 'products', element: <Products /> },
		{ to: 'upcoming', element: <Wishist /> },
		{ to: 'brands', element: <Brands /> },
		{ to: 'category', element: <Category /> },
		{ to: 'updatecategory/:_id', element: <UpdateCategory /> },
		{ to: 'orders', element: <Orders /> },
		{ to: 'orders/:id', element: <InvoicePage /> },
		{ to: 'orderstatus', element: <OrderStatus /> },
		{ to: 'users', element: <Users /> },
		{ to: 'coupons', element: <Coupons /> },
		{ to: 'transection', element: <Transection /> },
		{ to: 'shippings', element: <Shippings /> },
		{ to: 'withdraws', element: <Withdraws /> },
		{ to: 'refunds', element: <Refunds /> },
		{ to: 'features', element: <Features /> },
		{ to: 'addfeatures', element: <AddFeatures /> },
		{ to: 'updatefeatures/:_id', element: <UpdateFeatures /> },
		{ to: 'contacts', element: <Contacts /> },
		{ to: 'footerpages', element: <FooterPages /> },
		{ to: 'settings', element: <Settings /> },
		{ to: 'addproducts', element: <AddProducts /> },
		{ to: 'updateproducts/:_id', element: <UpdateProducts /> },
		{ to: 'addbrands', element: <AddBrands /> },
		{ to: 'addcategory', element: <AddCategory /> },
		{ to: 'addcoupons', element: <AddCoupons /> },
		{ to: 'updatecoupons/:_id', element: <UpdateCoupons /> },
		{ to: 'createaccount', element: <CreateAccount /> },
	];

	return (
		<>

			<AuthProvider>

				<BrowserRouter>

					<Routes>

						<Route path='/' element={<AdminLogin />} />
						<Route path='/login' element={<AdminLogin />} />
						<Route path='/dashboard/*' element={<AdminRoute><IndexDashboard /></AdminRoute>}>

							{
								paths?.map((path, index) => <Route key={index} path={path?.to} element={path?.element} />)
							}

						</Route>

					</Routes>

				</BrowserRouter>

			</AuthProvider>

		</>
	);
}

export default App;
