import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { BsEye, BsEyeSlash } from 'react-icons/bs';
import { useNavigate } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';

const AdminLogin = () => {

    const [eye, setEye] = useState(true);
    const [logo, setLogo] = useState('');
    const emailRef = useRef('');
    const passwordRef = useRef('');
    const { signInWithEmailPassword } = useAuth();
    const navigate = useNavigate();

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/logo')
            .then(res => setLogo(res.data[0].logo));

    }, []);


    const handleLogin = (e) => {
        e.preventDefault();

        const email = emailRef.current.value;
        const password = passwordRef.current.value;

        signInWithEmailPassword(email, password, navigate);

        e.target.reset();

    };

    return (
        <div className='bg-gray-100'>

            <div className='container px-8 lg:px-0'>

                <div className='flex h-screen items-center justify-center lg:w-5/6 mx-auto'>

                    <div className='border bg-white rounded-xl w-full lg:w-1/3 p-5 lg:p-10 flex flex-col gap-y-10'>

                        <div className='text-center mx-auto flex flex-col items-center gap-y-3'>
                            <img width="150px" src={logo} alt="brand" />
                            <h1 className='italic text-gray-500'>Admin Login to Dashboard</h1>
                        </div>

                        <form onSubmit={handleLogin} className='flex flex-col gap-y-3'>

                            <div>
                                <label htmlFor="email">Email</label>
                                <input  ref={emailRef} className='input' type="email" required />
                            </div>

                            <div>
                                <label htmlFor="password">Password</label>
                                <div className='relative'>

                                    <div onClick={() => setEye(!eye)} className='p-1 absolute right-3 top-5 cursor-pointer'>
                                        {
                                            eye ? <BsEyeSlash className='text-xl' /> : <BsEye className='text-xl' />
                                        }
                                    </div>
                                    <input  ref={passwordRef} className='input' type={eye ? 'password' : 'text'} required />

                                </div>
                            </div>

                            <button className='submit_btn mt-4' type='submit'>Login</button>

                        </form>

                    </div>

                </div>

            </div>

        </div>
    );
};

export default AdminLogin;
