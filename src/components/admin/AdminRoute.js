import { Navigate, useLocation } from 'react-router-dom';
import PuffLoader from 'react-spinners/PuffLoader';
import useAuth from '../../hooks/useAuth';
function AdminRoute({ children }) {

    let { user, loading } = useAuth();
    let location = useLocation();

    if (loading) {
        return <div className="flex justify-center items-center h-screen">
            <PuffLoader color={'#3B82F6'} size={200} />
        </div>;
    }

    if (!user?.email) {
        return <Navigate to="/login" state={{ from: location }} />;
    }

    return children;
}

export default AdminRoute;