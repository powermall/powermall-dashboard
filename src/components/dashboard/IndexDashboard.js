import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Toolbar from '@mui/material/Toolbar';
import axios from 'axios';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { BsBag, BsBox, BsCheckCircle, BsGift, BsThreeDotsVertical, BsTruck } from 'react-icons/bs';
import { FaBoxes, FaTicketAlt } from 'react-icons/fa';
import { FiLogOut, FiMessageSquare, FiSettings, FiUserPlus } from 'react-icons/fi';
import { HiMenuAlt2 } from 'react-icons/hi';
import { MdOutlineCategory, MdOutlineDashboard, MdOutlineFeaturedPlayList, MdOutlineGroups } from 'react-icons/md';
import { RiPagesLine, RiRefund2Line } from 'react-icons/ri';
import { Link, NavLink, Outlet, useNavigate } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';

const drawerWidth = 250;

function IndexDashboard(props) {
    const { window } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [logo, setLogo] = useState([]);
    const [role, setRole] = useState('');
    const [menus, setMenus] = useState([]);
    const { user } = useAuth();

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/logo')
            .then(res => setLogo(res.data));

        axios.get(`https://api.powermall.com.bd/roles/${user?.email}`)
            .then(res => setRole(res.data.role));

    }, [user?.email]);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    useEffect(() => {

        if (role === 'Admin') {

            setMenus([

                { name: 'Sales', icon: <MdOutlineDashboard />, to: 'sales' },
                { name: 'Products', icon: <BsBox />, to: 'products' },
                { name: 'Upcoming', icon: <BsBox />, to: 'upcoming' },
                { name: 'Brands', icon: <FaBoxes />, to: 'brands' },
                { name: 'Catagory', icon: <MdOutlineCategory />, to: 'category' },
                { name: 'Orders', icon: <BsBag />, to: 'orders' },
                { name: 'Order Status', icon: <BsCheckCircle />, to: 'orderstatus' },
                { name: 'Shippings', icon: <BsTruck />, to: 'shippings' },
                { name: 'Users', icon: <MdOutlineGroups />, to: 'users' },
                { name: 'Coupons', icon: <BsGift />, to: 'coupons' },
                { name: 'Transection', icon: <FaTicketAlt />, to: 'transection' },
                { name: 'Refunds', icon: <RiRefund2Line />, to: 'refunds' },
                { name: 'Features', icon: <MdOutlineFeaturedPlayList />, to: 'features' },
                { name: 'Contacts', icon: <FiMessageSquare />, to: 'contacts' },
                { name: 'Footer Pages', icon: <RiPagesLine />, to: 'footerpages' },
                { name: 'Settings', icon: <FiSettings />, to: 'settings' },

            ]);

        } else if (role === 'Manager') {

            setMenus([

                { name: 'Sales', icon: <MdOutlineDashboard />, to: 'sales' },
                { name: 'Products', icon: <BsBox />, to: 'products' },
                { name: 'Brands', icon: <FaBoxes />, to: 'brands' },
                { name: 'Catagory', icon: <MdOutlineCategory />, to: 'category' },
                { name: 'Orders', icon: <BsBag />, to: 'orders' },
                { name: 'Order Status', icon: <BsCheckCircle />, to: 'orderstatus' },
                { name: 'Shippings', icon: <BsTruck />, to: 'shippings' },
                { name: 'Contacts', icon: <FiMessageSquare />, to: 'contacts' },
                { name: 'Settings', icon: <FiSettings />, to: 'settings' },


            ]);

        } else if (role === 'Contributor') {

            setMenus([

                { name: 'Products', icon: <BsBox />, to: 'products' },
                { name: 'Brands', icon: <FaBoxes />, to: 'brands' },
                { name: 'Catagory', icon: <MdOutlineCategory />, to: 'category' },
                { name: 'Features', icon: <MdOutlineFeaturedPlayList />, to: 'features' },
                { name: 'Footer Pages', icon: <RiPagesLine />, to: 'footerpages' },
                { name: 'Settings', icon: <FiSettings />, to: 'settings' },

            ]);

        }

    }, [role]);

    const drawer = (
        <div>

            <div className='flex justify-center py-2 mt-1'>
                <NavLink to="/dashboard/sales">
                    <img className="w-60 h-12 object-contain cursor-pointer" src={logo[0]?.logo} alt="brand" />
                </NavLink>
            </div>

            <div className='pt-5'>

                {
                    menus?.map((menu, index) => <div key={index}>

                        <NavLink to={menu?.to} className={({ isActive }) => isActive ? 'text-blue-700' : ''}>

                            <ListItem button >
                                <div className='flex items-center gap-x-3 pl-8'>
                                    <div className='text-xl'>{menu?.icon}</div>
                                    <ListItemText>
                                        <h1>{menu?.name}</h1>
                                    </ListItemText>
                                </div>
                            </ListItem>

                        </NavLink>

                    </div>)
                }

            </div>

        </div >
    );

    const container = window !== undefined ? () => window().document.body : undefined;

    return (

        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar
                position="fixed"
                sx={{
                    width: { sm: `calc(100% - ${drawerWidth}px)` },
                    ml: { sm: `${drawerWidth}px` },
                    backgroundColor: 'white',
                    boxShadow: '0px 8px 10px #ddd'
                }}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ mr: 2, display: { sm: 'none' } }}
                    >
                        <HiMenuAlt2 className='text-black text-3xl' />
                    </IconButton>

                    <div className='text-black w-full'>

                        <div className='flex items-center justify-end gap-x-5'>
                            <div className='border rounded-full'>
                                <img className='w-10 rounded-full' src='https://statinfer.com/wp-content/uploads/dummy-user.png' alt="user" />
                            </div>
                            <h1 className='font-semibold'>{role}</h1>
                            <BasicMenu />
                        </div>

                    </div>

                </Toolbar>
            </AppBar>
            <Box
                component="nav"
                sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
                aria-label="mailbox folders"
            >
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Drawer
                    container={container}
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: 'block', sm: 'none' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                >
                    {drawer}
                </Drawer>
                <Drawer
                    variant="permanent"
                    sx={{
                        display: { xs: 'none', sm: 'block' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                    open
                >
                    {drawer}
                </Drawer>
            </Box>
            <Box
                component="main"
                sx={{ flexGrow: 1, p: 3, width: { sm: `calc(100% - ${drawerWidth}px)` } }}
            >
                <Toolbar />
                <Outlet />
            </Box>
        </Box>
    );
}

IndexDashboard.propTypes = {
    window: PropTypes.func,
};

function BasicMenu() {

    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const { SignOut } = useAuth();
    const navigate = useNavigate();

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>

            <div
                className='p-3 border-gray-200 hover:bg-gray-100 rounded-full cursor-pointer'
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
            >
                <BsThreeDotsVertical className='text-xl' />
            </div>
            <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
                sx={{ marginTop: '10px', marginLeft: '-20px' }}
            >

                <MenuItem><Link to='/dashboard/createaccount'><div className='flex items-center gap-x-3'><FiUserPlus className='text-xl' />Create Account</div></Link></MenuItem>
                <MenuItem onClick={() => { handleClose(); SignOut(navigate); }}><div className='flex items-center gap-x-3'><FiLogOut className='text-xl' />Logout</div></MenuItem>

            </Menu>

        </div>
    );
}

export default IndexDashboard;
