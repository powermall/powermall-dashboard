import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import Swal from 'sweetalert2';

const AddFeatures = () => {

    const [files, setFiles] = useState([]);
    const [cards, setCards] = useState([]);
    const titleRef = useRef('');
    const detailRef = useRef('');
    const btnNameRef = useRef('');
    const tagRef = useRef('');

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/headerbanner')
            .then(res => setCards(res.data));

    }, [cards]);

    const { getRootProps, getInputProps } = useDropzone({
        accept: "image/*",
        onDrop: (acceptedFiles) => {
            setFiles(
                acceptedFiles.map((file) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    })
                )
            );
        },
    });

    const image = files.map((file) => (
        <div key={file.name}>
            <div>
                <img className='rounded-xl border-2 w-40' src={file.preview} alt="preview" />
                <h1 className='text-gray-600'>{file.name}</h1>
            </div>
        </div>
    ));

    const handleBannerUpload = (e) => {
        e.preventDefault();

        const formdata1 = new FormData();
        formdata1.append('image', files?.[0]);

        if (files.length === 1) {

            axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata1)

                .then((response) => {

                    if (response.status === 200) {
                        const title = titleRef.current.value;
                        const details = detailRef.current.value;
                        const name = btnNameRef.current.value;
                        const tag = tagRef.current.value;
                        const img = response.data.data.image.url;
                        const data = { title, details, name, tag, img };

                        axios.post('https://api.powermall.com.bd/feature', data)
                            .then(res => {
                                if (res.status === 200) {
                                    e.target.reset();
                                    setFiles([]);
                                    Swal.fire(
                                        'Successful',
                                        'Feature Added Successfully',
                                        'success'
                                    );
                                }
                            });

                    }

                })
                .catch((error) => {
                    Swal.fire(
                        'Error',
                        'Reload The Page And Try Again',
                        'error'
                    );
                });

        } else {
            Swal.fire(
                'Upload Image',
                'Browse And Upload A Single Image',
                'error'
            );
        }

    };

    return (
        <div>
            <div>
                <div className="md:grid md:grid-cols-2 md:gap-6">
                    <div className="col-span-2 text-center">
                        <div className="px-4 sm:px-0">
                            <h3 className="text-2xl font-medium leading-6 text-gray-900">Features Card</h3>
                            <div className='h1 h-1 w-14 rounded-full bg-blue-600 mx-auto mt-3'></div>
                        </div>
                    </div>
                    <div className="mt-5 col-span-2 w-4/6 mx-auto">

                        <form onSubmit={handleBannerUpload}>
                            <div className="shadow-lg sm:rounded-md sm:overflow-hidden">
                                <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                                    <div className="grid grid-cols-6 gap-6">
                                        <div className="col-span-6 sm:col-span-3">
                                            <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                Card Title
                                            </label>
                                            <input
                                                required
                                                ref={titleRef}
                                                type="name"
                                                name="first-name"
                                                id="first-name"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-3">
                                            <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                                                Card Details
                                            </label>
                                            <input
                                                required
                                                ref={detailRef}
                                                type="text"
                                                name="last-name"
                                                id="last-name"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-6">
                                            <label htmlFor="email-address" className="block text-sm font-medium text-gray-700">
                                                Button Name
                                            </label>
                                            <input
                                                required
                                                ref={btnNameRef}
                                                type="text"
                                                name="email-address"
                                                id="email-address"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-6 lg:col-span-6">
                                            <label htmlFor="city" className="block text-sm font-medium text-gray-700">
                                               Tag (Must be small letters)
                                            </label>
                                            <input
                                                required
                                                ref={tagRef}
                                                type="text"
                                                name="city"
                                                id="city"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                    </div>

                                    <div>
                                        <label className="block text-sm font-medium text-gray-700 mb-4">Header Banner</label>
                                        <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                            <div className="space-y-1 text-center">
                                                <svg
                                                    className="mx-auto h-12 w-12 text-gray-400"
                                                    stroke="currentColor"
                                                    fill="none"
                                                    viewBox="0 0 48 48"
                                                    aria-hidden="true"
                                                >
                                                    <path
                                                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                                        strokeWidth={2}
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                                <div {...getRootProps()} className="flex text-sm justify-center text-gray-600">
                                                    <label
                                                        htmlFor="file-upload"
                                                        className="relative cursor-pointer bg-white rounded-md font-medium text-blue-600 hover:text-blue-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-blue-500"
                                                    >
                                                        <span>Upload a file</span>
                                                        <input {...getInputProps()} className="sr-only" />
                                                    </label>
                                                    <p className="pl-1">or drag and drop</p>
                                                </div>
                                                <p className="text-xs text-gray-500 pb-5">PNG, JPG up to 10MB</p>
                                                <div className="uploaded__image flex justify-center gap-x-2">{image}</div>

                                                {/* {

                                                    image?.length !== 0 ? (

                                                        <div className="uploaded__image flex gap-x-2">{image}</div>

                                                    ) : (

                                                        <div className='flex gap-x-5 items-center'>
                                                            <img className="w-28 rounded-lg mx-auto" src={img?.[0]} alt="product-img" />
                                                            <img className="w-28 rounded-lg mx-auto" src={img?.[1]} alt="product-img" />
                                                            <img className="w-28 rounded-lg mx-auto" src={img?.[2]} alt="product-img" />
                                                        </div>

                                                    )

                                                } */}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    <button
                                        type="submit"
                                        className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                    >
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AddFeatures;
