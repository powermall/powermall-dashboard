import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import Swal from 'sweetalert2';

const HeaderBannerUpload = () => {

    return (
        <div className='mb-20 lg:mb-0'>
            <div>
                <div className="grid grid-cols-5">
                    <div className="col-span-5">
                        <div className="px-4 sm:px-0 text-center mb-5">
                            <h3 className="text-xl font-medium leading-6 text-gray-900">Header Banner</h3>
                            <div className='w-14 h-1 mx-auto rounded-full bg-blue-500 my-3'></div>
                            <p className="mt-1 text-sm text-gray-600">
                                Upload your site header banner from here. <br />
                                Dimension of the banner should be 1920x800 Pixel
                            </p>
                        </div>
                    </div>
                    <div className="mt-5 md:mt-0 col-span-5">

                        <div className='grid grid-cols-1 lg:grid-cols-3 gap-10'>

                            <Banner id='61cb8a1b7d2f7a97480ed4b6' num='1' />
                            <Banner id='61cb93119190ff571b274a19' num='2' />
                            <Banner id='61cb94d79190ff571b274a1a' num='3' />

                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export const Banner = ({ id, num }) => {

    const [files, setFiles] = useState([]);
    const [banner, setBanner] = useState({});
    const [disabled, setDisabled] = useState(false);
    const linkRef = useRef('');

    useEffect(() => {

        axios.get(`https://api.powermall.com.bd/headerbanner/${id}`)
            .then(res => setBanner(res.data));

    }, [id]);

    const { getRootProps, getInputProps } = useDropzone({
        accept: "image/*",
        onDrop: (acceptedFiles) => {
            setFiles(
                acceptedFiles.map((file) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    })
                )
            );
        },
    });

    const image = files.map((file) => (
        <div key={file.name}>
            <div>
                <img className='rounded-xl border-2 w-60' src={file.preview} alt="preview" />
                <h1 className='text-gray-600'>{file.name}</h1>
            </div>
        </div>
    ));

    const handleLogoUpload = (e) => {
        e.preventDefault();

        if (files.length === 1) {

            const formdata1 = new FormData();
            formdata1.append('image', files?.[0]);

            axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata1)

                .then((response) => {

                    if (response.status === 200) {

                        const img = response.data.data.image.url;
                        const link = linkRef.current.value;
                        const data = { link, img };

                        axios.put(`https://api.powermall.com.bd/headerbanner/${id}`, data)
                            .then(res => {

                                if (res.data.modifiedCount > 0) {

                                    Swal.fire('Successful', 'Banner Updated Successfully', 'success');
                                    setDisabled(false);

                                }

                            });

                    }

                })
                .catch((error) => {
                    Swal.fire(
                        'Error',
                        'Reload The Page And Try Again',
                        'error'
                    );
                    setDisabled(false);
                });
        }
        else if (files.length === 0) {

            const link = linkRef.current.value;
            axios.put(`https://api.powermall.com.bd/headerbanner/${id}`, { link })
                .then(res => {

                    if (res.data.modifiedCount > 0) {

                        Swal.fire('Successful', 'Banner Updated Successfully', 'success');
                        setDisabled(false);

                    }

                });

        }
        else if (files.length > 1) {
            Swal.fire(
                'Error',
                'You Can Upload Only 1 Image',
                'error'
            );
            setDisabled(false);
        }

    };

    return (

        <form onSubmit={handleLogoUpload} onChange={() => setDisabled(true)}>
            <div className="shadow-lg sm:rounded-md sm:overflow-hidden">
                <div className="px-4 py-5 bg-white space-y-6 sm:p-6">

                    <div>
                        <label className="block text-sm font-medium text-gray-700 mb-2 text-center">Banner {num}</label>
                        <div className='h-1 w-10 mx-auto bg-blue-500 mb-4'></div>
                        <div>
                            <input defaultValue={banner?.link} ref={linkRef} className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300 mb-3" type="text" placeholder='Redirect Link' required />
                        </div>
                        <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                            <div className="space-y-1 text-center">
                                <svg
                                    className="mx-auto h-12 w-12 text-gray-400"
                                    stroke="currentColor"
                                    fill="none"
                                    viewBox="0 0 48 48"
                                    aria-hidden="true"
                                >
                                    <path
                                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                        strokeWidth={2}
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                </svg>
                                <div {...getRootProps()} className="flex flex-col lg:flex-row text-sm justify-center text-gray-600">
                                    <label
                                        htmlFor="file-upload"
                                        className="relative cursor-pointer bg-white rounded-md font-medium text-blue-600 hover:text-blue-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-blue-500"
                                    >
                                        <span>Upload a file</span>
                                        <input {...getInputProps()} className="sr-only" />
                                    </label>
                                    <p className="pl-1">or drag and drop</p>
                                </div>
                                <p className="text-xs text-gray-500 pb-5">PNG, JPG up to 10MB</p>

                                {

                                    image?.length !== 0 ? (

                                        <div className="uploaded__image flex gap-x-2">{image}</div>

                                    ) : (

                                        <img className="w-60 rounded-lg mx-auto" src={banner?.img} alt="product-img" />

                                    )

                                }

                            </div>
                        </div>
                    </div>
                </div>
                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                    {

                        disabled ? (

                            <button
                                onClick={handleLogoUpload}
                                type="submit"
                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                            >
                                Save
                            </button>

                        ) : (

                            <button
                                disabled
                                type="submit"
                                className="opacity-20 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 cursor-not-allowed"
                            >
                                Save
                            </button>

                        )

                    }
                </div>
            </div>
        </form>

    );

};


export default HeaderBannerUpload;
