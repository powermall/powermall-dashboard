import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import Swal from 'sweetalert2';
import AppLinks from './additional-pages/AppLinks';
import Favicon from './additional-pages/Favicon';
import Copyright from './Copyright';
import HeaderBannerUpload from './HeaderBannerUpload';
import Notice from './Notice';
import NotificationBannerUpload from './NotificationBannerUpload';
import RecomendedBannerUpload from './RecomendedBannerUpload';
import Seo from './Seo';
import SocialLinks from './SocialLinks';
import SpecialSaleBannerUpload from './SpecialSaleBannerUpload';

const Settings = () => {

    const [files, setFiles] = useState([]);
    const [logo, setLogo] = useState([]);
    const [info, setInfo] = useState([]);
    const emailRef = useRef('');
    const phoneRef = useRef('');
    const addressRef = useRef('');
    const delivaryTimeRef = useRef('');
    const inDelivaryCostRef = useRef('');
    const outDelivaryCostRef = useRef('');
    const globalRef = useRef('');
    const dreoneRef = useRef('');
    const autoRef = useRef('');
    const productReturnRef = useRef('');
    const [disabled, setDisabled] = useState(false);

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/logo')
            .then(res => { setLogo(res.data); });

        axios.get('https://api.powermall.com.bd/information')
            .then(res => setInfo(res.data));

    }, []);



    const { getRootProps, getInputProps } = useDropzone({
        accept: "image/*",
        onDrop: (acceptedFiles) => {
            setFiles(
                acceptedFiles.map((file) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    })
                )
            );
        },
    });

    const image = files.map((file) => (
        <div key={file.name}>
            <div>
                <img className='rounded-xl border-2 w-60' src={file.preview} alt="preview" />
                <h1 className='text-gray-600'>{file.name}</h1>
            </div>
        </div>
    ));

    const handleLogoUpload = (e) => {
        e.preventDefault();

        if (files.length === 1) {

            axios.delete('https://api.powermall.com.bd/logo')
                .then(res => {
                    if (res.status === 200) {

                        const formdata1 = new FormData();
                        const formdata2 = new FormData();
                        formdata1.append('image', files?.[0]);

                        axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata1)

                            .then((response) => {

                                if (response.status === 200) {

                                    const logo = response.data.data.image.url;
                                    const name = 'logo';
                                    formdata2.append('name', name);
                                    formdata2.append('logo', logo);

                                    axios.post('https://api.powermall.com.bd/logo', formdata2)
                                        .then(res => {
                                            if (res.status === 200) {
                                                Swal.fire(
                                                    'Successful',
                                                    'Logo Changed Successfully',
                                                    'success'
                                                );
                                                setDisabled(false);
                                            }
                                        });

                                }

                            })
                            .catch((error) => {
                                Swal.fire(
                                    'Error',
                                    'Reload The Page And Try Again',
                                    'error'
                                );
                            });

                    }
                });

        } else if (files.length === 0) {

            Swal.fire(
                'Error',
                'You Must Have To Upload 1 image',
                'error'
            );

        } else if (files.length > 1) {
            Swal.fire(
                'Error',
                'You Can Upload Only 1 Image',
                'error'
            );
        }

    };

    const handleInformation = (e) => {
        e.preventDefault();

        const email = emailRef.current.value;
        const phone = phoneRef.current.value;
        const address = addressRef.current.value;
        const duration = delivaryTimeRef.current.value;
        const insidecost = inDelivaryCostRef.current.value;
        const outsidecost = outDelivaryCostRef.current.value;
        const globalcost = globalRef.current.value;
        const dronecost = dreoneRef.current.value;
        const automotivecost = autoRef.current.value;
        const policy = productReturnRef.current.value;
        const data = { email, phone, address, duration, insidecost, outsidecost, globalcost, dronecost, automotivecost, policy };

        axios.put('https://api.powermall.com.bd/information/61cb7c0bba7f533d505060dd', data)
            .then(res => {

                if (res.data.modifiedCount > 0) {
                    Swal.fire('Successful', 'Information Updated Successfully', 'success');
                    setDisabled(false);
                }

            });

    };

    return (
        <div className='my-10'>

            <div className='mb-20 lg:mb-0'>
                <div className="md:grid md:grid-cols-3 md:gap-6">
                    <div className="md:col-span-3 text-center">
                        <div className="px-4 sm:px-0">
                            <h3 className="text-xl font-medium leading-6 text-gray-900">Logo & Favicon</h3>
                            <div className='w-16 h-1 rounded-full bg-blue-500 my-3 mx-auto'></div>
                            <p className="mt-1 text-sm text-gray-600">
                                Upload your site logo from here. <br />
                                Dimension of the logo should be  128x40 Pixel
                            </p>
                        </div>
                    </div>
                    <div className="mt-5 md:mt-0 md:col-span-3">
                        <div className="grid grid-cols-1 lg:grid-cols-2 gap-x-10">

                            <form onSubmit={handleLogoUpload} onChange={() => setDisabled(true)}>
                                <div className="shadow-lg sm:rounded-md sm:overflow-hidden">
                                    <div className="px-4 py-5 bg-white space-y-6 sm:p-6">

                                        <div>
                                            <label className="block text-sm font-medium text-gray-700 mb-4">Company Logo</label>
                                            <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                                <div className="space-y-1 text-center">
                                                    <svg
                                                        className="mx-auto h-12 w-12 text-gray-400"
                                                        stroke="currentColor"
                                                        fill="none"
                                                        viewBox="0 0 48 48"
                                                        aria-hidden="true"
                                                    >
                                                        <path
                                                            d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                                            strokeWidth={2}
                                                            strokeLinecap="round"
                                                            strokeLinejoin="round"
                                                        />
                                                    </svg>
                                                    <div {...getRootProps()} className="flex flex-col lg:flex-row text-sm justify-center text-gray-600">
                                                        <label
                                                            htmlFor="file-upload"
                                                            className="relative cursor-pointer bg-white rounded-md font-medium text-blue-600 hover:text-blue-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-blue-500"
                                                        >
                                                            <span>Upload a file</span>
                                                            <input {...getInputProps()} className="sr-only" />
                                                        </label>
                                                        <p className="pl-1">or drag and drop</p>
                                                    </div>
                                                    <p className="text-xs text-gray-500 pb-5">PNG, JPG up to 10MB</p>

                                                    {

                                                        image?.length !== 0 ? (

                                                            <div className="uploaded__image flex gap-x-2">{image}</div>

                                                        ) : (

                                                            <img className="w-60 rounded-lg mx-auto" src={logo[0]?.logo} alt="product-img" />

                                                        )

                                                    }

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                        {

                                            disabled ? (

                                                <button
                                                    onClick={handleLogoUpload}
                                                    type="submit"
                                                    className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                                >
                                                    Save
                                                </button>

                                            ) : (

                                                <button
                                                    disabled
                                                    type="submit"
                                                    className="opacity-20 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 cursor-not-allowed"
                                                >
                                                    Save
                                                </button>

                                            )

                                        }
                                    </div>
                                </div>
                            </form>

                            <Favicon />

                        </div>
                    </div>
                </div>
            </div>


            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-20">
                    <div className="border-t border-gray-200" />
                </div>
            </div>


            <HeaderBannerUpload />

            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-20">
                    <div className="border-t border-gray-200" />
                </div>
            </div>

            <RecomendedBannerUpload />

            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-20">
                    <div className="border-t border-gray-200" />
                </div>
            </div>

            <SpecialSaleBannerUpload />

            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-20">
                    <div className="border-t border-gray-200" />
                </div>
            </div>

            <NotificationBannerUpload />

            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-20">
                    <div className="border-t border-gray-200" />
                </div>
            </div>

            <Notice />

            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-20">
                    <div className="border-t border-gray-200" />
                </div>
            </div>

            <Seo />

            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-20">
                    <div className="border-t border-gray-200" />
                </div>
            </div>


            <AppLinks />


            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-20">
                    <div className="border-t border-gray-200" />
                </div>
            </div>

            <SocialLinks />

            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-20">
                    <div className="border-t border-gray-200" />
                </div>
            </div>

            <Copyright />

            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-20">
                    <div className="border-t border-gray-200" />
                </div>
            </div>

            <div className="mt-10 sm:mt-0">
                <div className="md:grid md:grid-cols-3 md:gap-6">
                    <div className="md:col-span-1">
                        <div className="px-4 sm:px-0">
                            <h3 className="text-xl font-medium leading-6 text-gray-900">Information</h3>
                            <div className='w-16 h-1 rounded-full bg-blue-500 my-3'></div>
                            <p className="mt-1 text-sm text-gray-600">Change your site information from here</p>
                        </div>
                    </div>
                    <div className="mt-5 md:mt-0 md:col-span-2">
                        <form onSubmit={handleInformation} onChange={() => setDisabled(true)}>
                            <div className="shadow-lg overflow-hidden sm:rounded-md">
                                <div className="px-4 py-5 bg-white sm:p-6">
                                    <div className="grid grid-cols-6 gap-6">
                                        <div className="col-span-6 sm:col-span-3">
                                            <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                Company Email Address
                                            </label>
                                            <input
                                                ref={emailRef}
                                                defaultValue={info[0]?.email}
                                                type="email"
                                                name="first-name"
                                                id="first-name"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-3">
                                            <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                                                Company Phone Number
                                            </label>
                                            <input
                                                ref={phoneRef}
                                                defaultValue={info[0]?.phone}
                                                type="text"
                                                name="last-name"
                                                id="last-name"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-6">
                                            <label htmlFor="email-address" className="block text-sm font-medium text-gray-700">
                                                Company Office Address
                                            </label>
                                            <input
                                                ref={addressRef}
                                                defaultValue={info[0]?.address}
                                                type="text"
                                                name="email-address"
                                                id="email-address"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-6 lg:col-span-2">
                                            <label htmlFor="city" className="block text-sm font-medium text-gray-700">
                                                Delivery Time
                                            </label>
                                            <input
                                                ref={delivaryTimeRef}
                                                defaultValue={info[0]?.duration}
                                                type="text"
                                                name="city"
                                                id="city"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                                            <label htmlFor="region" className="block text-sm font-medium text-gray-700">
                                                Inside Dhaka Delivery Cost
                                            </label>
                                            <input
                                                ref={inDelivaryCostRef}
                                                defaultValue={info[0]?.insidecost}
                                                type="text"
                                                name="region"
                                                id="region"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                                            <label htmlFor="region" className="block text-sm font-medium text-gray-700">
                                                Outside Dhaka Delivery Cost
                                            </label>
                                            <input
                                                ref={outDelivaryCostRef}
                                                defaultValue={info[0]?.outsidecost}
                                                type="text"
                                                name="region"
                                                id="region"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>
                                        <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                                            <label htmlFor="region" className="block text-sm font-medium text-gray-700">
                                                Global Putchase
                                            </label>
                                            <input
                                                ref={globalRef}
                                                defaultValue={info[0]?.globalcost}
                                                type="text"
                                                name="region"
                                                id="region"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>
                                        <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                                            <label htmlFor="region" className="block text-sm font-medium text-gray-700">
                                                Drone Service
                                            </label>
                                            <input
                                                ref={dreoneRef}
                                                defaultValue={info[0]?.dronecost}
                                                type="text"
                                                name="region"
                                                id="region"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>
                                        <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                                            <label htmlFor="region" className="block text-sm font-medium text-gray-700">
                                                Automotive
                                            </label>
                                            <input
                                                ref={autoRef}
                                                defaultValue={info[0]?.automotivecost}
                                                type="text"
                                                name="region"
                                                id="region"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-3 lg:col-span-6">
                                            <label htmlFor="postal-code" className="block text-sm font-medium text-gray-700">
                                                Product Return Days
                                            </label>
                                            <input
                                                ref={productReturnRef}
                                                defaultValue={info[0]?.policy}
                                                type="text"
                                                name="postal-code"
                                                id="postal-code"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    {

                                        disabled ? (

                                            <button
                                                onClick={handleInformation}
                                                type="submit"
                                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                            >
                                                Save
                                            </button>

                                        ) : (

                                            <button
                                                disabled
                                                type="submit"
                                                className="opacity-20 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 cursor-not-allowed"
                                            >
                                                Save
                                            </button>

                                        )

                                    }
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default Settings;