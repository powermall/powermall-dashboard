import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import Swal from 'sweetalert2';

const Seo = () => {

    const [files, setFiles] = useState([]);
    const [seo, setSeo] = useState([]);
    const [disabled, setDisabled] = useState(false);
    const titleRef = useRef('');
    const descriptionRef = useRef('');
    const urlRef = useRef('');
    const ogTitleRef = useRef('');
    const ogDescriptionRef = useRef('');

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/seo')
            .then(res => setSeo(res.data[0]));

    }, [seo]);

    const handleSeo = (e) => {
        e.preventDefault();

        if (files.length === 1) {

            const formdata1 = new FormData();
            formdata1.append('image', files?.[0]);

            axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata1)

                .then((response) => {

                    if (response.status === 200) {

                        const img = response.data.data.image.url;
                        const title = titleRef.current.value;
                        const description = descriptionRef.current.value;
                        const url = urlRef.current.value;
                        const ogTitle = ogTitleRef.current.value;
                        const ogDescription = ogDescriptionRef.current.value;
                        const data = { title, description, url, ogTitle, ogDescription, img };

                        axios.put('https://api.powermall.com.bd/seo/61c758f403925425ace311fa', data)
                            .then(res => {
                                if (res.data.modifiedCount > 0) {
                                    Swal.fire(
                                        'Successful',
                                        'SEO Changed Successfully',
                                        'success'
                                    );
                                    setDisabled(false);
                                }
                            });

                    }

                })
                .catch((error) => {
                    Swal.fire(
                        'Error',
                        'Reload The Page And Try Again',
                        'error'
                    );
                    setDisabled(false);
                });

        } else if (files.length === 0) {

            const img = seo?.img;
            const title = titleRef.current.value;
            const description = descriptionRef.current.value;
            const url = urlRef.current.value;
            const ogTitle = ogTitleRef.current.value;
            const ogDescription = ogDescriptionRef.current.value;
            const data = { title, description, url, ogTitle, ogDescription, img };

            axios.put('https://api.powermall.com.bd/seo/61c758f403925425ace311fa', data)
                .then(res => {
                    if (res.data.modifiedCount > 0) {
                        Swal.fire(
                            'Successful',
                            'SEO Changed Successfully',
                            'success'
                        );
                        setDisabled(false);
                    }
                });

        } else if (files.length > 1) {
            Swal.fire(
                'Error',
                'You Can Upload Only 1 Image',
                'error'
            );
        }

    };

    const { getRootProps, getInputProps } = useDropzone({
        accept: "image/*",
        onDrop: (acceptedFiles) => {
            setFiles(
                acceptedFiles.map((file) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    })
                )
            );
        },
    });

    const image = files.map((file) => (
        <div key={file.name}>
            <div>
                <img className='rounded-xl border-2 w-60' src={file.preview} alt="preview" />
                <h1 className='text-gray-600'>{file.name}</h1>
            </div>
        </div>
    ));

    return (
        <div>
            <div className="mt-10 sm:mt-0">
                <div className="md:grid md:grid-cols-3 md:gap-6">
                    <div className="md:col-span-1">
                        <div className="px-4 sm:px-0">
                            <h3 className="text-xl font-medium leading-6 text-gray-900">SEO</h3>
                            <div className='w-8 h-1 rounded-full bg-blue-500 my-3'></div>
                            <p className="mt-1 text-sm text-gray-600">Change your site SEO from here</p>
                        </div>
                    </div>
                    <div className="mt-5 md:mt-0 md:col-span-2">
                        <form onSubmit={handleSeo} onChange={() => setDisabled(true)}>
                            <div className="shadow-lg overflow-hidden sm:rounded-md">
                                <div className="px-4 py-5 bg-white sm:p-6">
                                    <div className="grid grid-cols-6 gap-6">
                                        <div className="col-span-6 sm:col-span-6">
                                            <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                Meta Title
                                            </label>
                                            <input
                                                ref={titleRef}
                                                defaultValue={seo?.title}
                                                type="text"
                                                name="first-name"
                                                id="first-name"
                                                className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-6">
                                            <label htmlFor="email-address" className="block text-sm font-medium text-gray-700">
                                                Meta Description
                                            </label>
                                            <textarea
                                                ref={descriptionRef}
                                                defaultValue={seo?.description}
                                                type="text"
                                                name="email-address"
                                                id="email-address"
                                                className="h-24 mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-6 lg:col-span-3">
                                            <label htmlFor="city" className="block text-sm font-medium text-gray-700">
                                                Canonical URL
                                            </label>
                                            <input
                                                ref={urlRef}
                                                defaultValue={seo?.url}
                                                type="text"
                                                name="city"
                                                id="city"
                                                className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-3 lg:col-span-3">
                                            <label htmlFor="region" className="block text-sm font-medium text-gray-700">
                                                OG Title
                                            </label>
                                            <input
                                                ref={ogTitleRef}
                                                defaultValue={seo?.ogTitle}
                                                type="text"
                                                name="region"
                                                id="region"
                                                className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-3 lg:col-span-6">
                                            <label htmlFor="postal-code" className="block text-sm font-medium text-gray-700">
                                                OG Description
                                            </label>
                                            <textarea
                                                ref={ogDescriptionRef}
                                                defaultValue={seo?.ogDescription}
                                                type="text"
                                                name="postal-code"
                                                id="postal-code"
                                                className="h-24 mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="bg-white space-y-6 col-span-6 w-full">

                                            <div>
                                                <label className="block text-sm font-medium text-gray-700">Image</label>
                                                <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                                    <div className="space-y-1 text-center">
                                                        <svg
                                                            className="mx-auto h-12 w-12 text-gray-400"
                                                            stroke="currentColor"
                                                            fill="none"
                                                            viewBox="0 0 48 48"
                                                            aria-hidden="true"
                                                        >
                                                            <path
                                                                d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                                                strokeWidth={2}
                                                                strokeLinecap="round"
                                                                strokeLinejoin="round"
                                                            />
                                                        </svg>
                                                        <div {...getRootProps()} className="flex text-sm justify-center text-gray-600">
                                                            <label
                                                                htmlFor="file-upload"
                                                                className="relative cursor-pointer bg-white rounded-md font-medium text-blue-600 hover:text-blue-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-blue-500"
                                                            >
                                                                <span>Upload a file</span>
                                                                <input {...getInputProps()} className="sr-only" />
                                                            </label>
                                                            <p className="pl-1">or drag and drop</p>
                                                        </div>
                                                        <p className="text-xs text-gray-500 pb-5">PNG, JPG up to 10MB</p>

                                                        {

                                                            image?.length !== 0 ? (

                                                                <div className="uploaded__image flex gap-x-2">{image}</div>

                                                            ) : (

                                                                <img className="w-60 rounded-lg mx-auto" src={seo?.img} alt="product-img" />

                                                            )

                                                        }

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    {

                                        disabled ? (

                                            <button
                                                onClick={handleSeo}
                                                type="submit"
                                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                            >
                                                Save
                                            </button>

                                        ) : (

                                            <button
                                                disabled
                                                type="submit"
                                                className="opacity-20 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 cursor-not-allowed"
                                            >
                                                Save
                                            </button>

                                        )

                                    }
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Seo;
