import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiOutlineDelete } from 'react-icons/ai';

const Transection = () => {

    const [transections, setTransections] = useState([]);

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/orders')
            .then(res => setTransections(res.data));

    }, []);

    const handleDelete = (_id) => {



    };

    return (
        <div>

            <div>

                <div className='grid grid-cols-1 md:grid-cols-2 px-5 py-8 border border-gray-200 rounded-lg items-center shadow-lg mb-10'>

                    <h1 className='font-medium text-xl mb-2 md:mb-2'>Tax</h1>

                </div>


                <div className="flex flex-col shadow-2xl">
                    <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-6">
                        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-6">
                            <div className="overflow-hidden border-b border-gray-200 sm:rounded-lg shadow">
                                <table className="min-w-full divide-y divide-gray-200">
                                    <thead className="bg-gray-50">
                                        <tr>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                ID
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Name
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Email
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Money
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Date
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Time
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Actions
                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody className="bg-white divide-y divide-gray-200 text-gray-600">
                                        {
                                            transections?.map((transection) => (

                                                <tr key={transection.email} className='hover:bg-gray-100 text-center'>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{transection?._id}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{transection?.name}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{transection?.email}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{transection?.price}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{transection?.date}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{transection?.time}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap text-sm text-gray-500">
                                                        <button onClick={() => handleDelete(transection?._id)} className='p-2 border rounded-full bg-red-500  hover:bg-red-700 text-white transition duration-500 shadow-lg'><AiOutlineDelete className='text-xl' /></button>
                                                    </td>

                                                </tr>

                                            ))
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Transection;
