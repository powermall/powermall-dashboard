import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiOutlineDelete } from 'react-icons/ai';
import { BsSearch } from 'react-icons/bs';
import Swal from 'sweetalert2';

const Users = () => {

    const [users, setUsers] = useState([]);
    const [displayUsers, setDisplayUsers] = useState([]);

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/users')
            .then(res => {
                setUsers(res.data);
                setDisplayUsers(res.data);
            });

    }, []);

    const handleDelete = (_id) => {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                axios.delete(`https://api.powermall.com.bd/users/${_id}`, _id)
                    .then(res => {
                        if (res.data.deletedCount > 0) {
                            const newUsers = displayUsers.filter(displayUser => displayUser?._id !== _id);
                            setDisplayUsers(newUsers);
                            setUsers(newUsers);
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                        }
                    });

            }
        });




    };

    const handleSearch = (e) => {

        const searchText = e.target.value;
        const matchedUsers = users?.filter(user => user?.name.toLowerCase().includes(searchText.toLowerCase()));
        setDisplayUsers(matchedUsers);

    };

    return (
        <div>

            <div>

                <div>

                    <div>

                        <div className='grid grid-cols-1 md:grid-cols-2 px-5 py-8 border border-gray-200 rounded-lg items-center shadow-lg mb-10'>

                            <h1 className='font-medium text-xl mb-2 md:mb-2'>Users</h1>
                            <div className='relative flex gap-x-4'>
                                <BsSearch className='absolute top-4 left-3 text-gray-600' />
                                <input onChange={handleSearch} className='px-10 py-3 border border-gray-200 rounded-lg outline-none focus:ring-2 w-full ring-blue-500 transition duration-300' type="text" placeholder='Search Users By There Name' />
                            </div>

                        </div>

                        <div className="flex flex-col shadow-2xl">
                            <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-6">
                                <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-6">
                                    <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                        <table className="min-w-full divide-y divide-gray-200">
                                            <thead className="bg-gray-50">
                                                <tr>
                                                    <th
                                                        scope="col"
                                                        className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                                    >
                                                        Avatar
                                                    </th>
                                                    <th
                                                        scope="col"
                                                        className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                                    >
                                                        Name
                                                    </th>
                                                    <th
                                                        scope="col"
                                                        className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                                    >
                                                        Email
                                                    </th>
                                                    <th
                                                        scope="col"
                                                        className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                                    >
                                                        Mobile
                                                    </th>
                                                    <th
                                                        scope="col"
                                                        className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                                    >
                                                        Role
                                                    </th>
                                                    <th
                                                        scope="col"
                                                        className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                                    >
                                                        Actions
                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody className="bg-white divide-y divide-gray-200">

                                                {displayUsers.length === 0 ? <tr>

                                                    <td colSpan={12} className='py-3'>

                                                        <h1 className='text-gray-400 font-medium text-sm text-center'>No Data Found</h1>

                                                    </td>

                                                </tr> : ''}

                                                {displayUsers.map((user, index) => (

                                                    <tr key={index} className='hover:bg-gray-100 text-center'>

                                                        <td className="px-6 py-2 whitespace-nowrap">

                                                            <img className='w-10 h-10 mx-auto rounded-full' src={user?.image} alt="user" />

                                                        </td>

                                                        <td className="px-6 py-2 whitespace-nowrap">
                                                            <div className="text-sm text-gray-900">{user?.name}</div>
                                                        </td>

                                                        <td className="px-6 py-2 whitespace-nowrap">
                                                            <h1>{user?.email}</h1>
                                                        </td>

                                                        <td className="px-6 py-2 whitespace-nowrap">
                                                            <h1>{user?.mobile}</h1>
                                                        </td>

                                                        <td className="px-6 py-2 whitespace-nowrap">
                                                            <h1>{user?.role}</h1>
                                                        </td>

                                                        <td className="px-6 py-2 whitespace-nowrap text-sm">
                                                            <div className='flex justify-center gap-x-2'>
                                                                <button onClick={() => handleDelete(user?._id)} className='p-2 border rounded-full  hover:bg-red-700 shadow-lg text-white bg-red-600'><AiOutlineDelete className='text-xl' /></button>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    );
};

export default Users;
