import axios from 'axios';
import React, { useRef, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import Swal from 'sweetalert2';

const AddBrands = () => {

    const brandNameRef = useRef('');
    const [files, setFiles] = useState([]);

    const { getRootProps, getInputProps } = useDropzone({
        accept: "image/*",
        onDrop: (acceptedFiles) => {
            setFiles(
                acceptedFiles.map((file) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    })
                )
            );
        },
    });

    const image = files.map((file) => (
        <div key={file.name}>
            <div>
                <img className='rounded-xl border-4 mt-10' src={file.preview} style={{ width: "100%", height: "160px", objectFit: "cover" }} alt="preview" />
                <h1 className='text-gray-600'>{file.name}</h1>
            </div>
        </div>
    ));

    const handleAddBrand = (e) => {
        e.preventDefault();

        const brand = brandNameRef.current.value;
        const formdata1 = new FormData();
        formdata1.append('image', files?.[0]);


        axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata1)

            .then((response) => {

                if (response.status === 200) {

                    const img = response.data.data.image.url;

                    axios.post('https://api.powermall.com.bd/brands', { brand, img })
                        .then(res => {
                            if (res.status === 200) {
                                Swal.fire(
                                    'Successful',
                                    'Brand Added Successfully',
                                    'success'
                                );
                                e.target.reset();
                            }
                        });

                }

            })
            .catch((error) => {
                Swal.fire(
                    'Error',
                    'Reload The Page And Try Again',
                    'error'
                );
            });

    };

    return (
        <div>
            <div>

                <div className="mt-10 sm:mt-0">
                    <div className="md:grid md:grid-cols-1 md:gap-6">
                        <div className="md:col-span-1 mt-5">
                            <div className="px-4 sm:px-0 text-center">
                                <h3 className="text-2xl font-medium leading-6 text-gray-900">Add Brands</h3>
                                <div className='w-20 h-1 rounded-full bg-blue-600 mx-auto mt-5'></div>
                            </div>
                        </div>
                        <div className="mt-5 md:mt-0 md:col-span-2 lg:w-5/6 mx-auto">
                            <form onSubmit={handleAddBrand}>
                                <div className="shadow-xl overflow-hidden sm:rounded-md">
                                    <div className="px-4 py-5 bg-white sm:p-6">
                                        <div className="grid grid-cols-6 gap-6">
                                            <div className="col-span-6 sm:col-span-12">
                                                <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                    Brand Name
                                                </label>
                                                <input
                                                    required
                                                    ref={brandNameRef}
                                                    type="text"
                                                    name="first-name"
                                                    id="first-name"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                        </div>
                                    </div>

                                    <div className="col-span-6">

                                        <div className="mt-5">

                                            <div className="sm:rounded-md sm:overflow-hidden">
                                                <div className="px-4 py-5 bg-white space-y-6 sm:p-6">

                                                    <div>
                                                        <label className="block text-sm font-medium text-gray-700">Brand Image</label>
                                                        <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                                            <div className="space-y-1 text-center">
                                                                <svg
                                                                    className="mx-auto h-12 w-12 text-gray-400"
                                                                    stroke="currentColor"
                                                                    fill="none"
                                                                    viewBox="0 0 48 48"
                                                                    aria-hidden="true"
                                                                >
                                                                    <path
                                                                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                                                        strokeWidth={2}
                                                                        strokeLinecap="round"
                                                                        strokeLinejoin="round"
                                                                    />
                                                                </svg>
                                                                <div {...getRootProps()} className="flex flex-col lg:flex-row justify-center text-sm text-gray-600">
                                                                    <label
                                                                        htmlFor="file-upload"
                                                                        className="relative cursor-pointer bg-white rounded-md font-medium text-blue-600 hover:text-blue-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-blue-500"
                                                                    >
                                                                        <span>Upload an Image</span>
                                                                        <input {...getInputProps()} className="sr-only" />
                                                                    </label>
                                                                    <p className="pl-1">or drag and drop</p>
                                                                    <p></p>
                                                                </div>
                                                                <p className="text-xs text-gray-500">PNG, JPG up to 2MB</p>

                                                                <div className="uploaded__image flex gap-x-2">{image}</div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                        <button
                                            type="submit"
                                            className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                        >
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    );
};

export default AddBrands;
