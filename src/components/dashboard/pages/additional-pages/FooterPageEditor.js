import React, { useEffect, useState } from 'react';
import { Editor } from '@tinymce/tinymce-react';
import axios from 'axios';

const FooterPageEditor = ({ api, name }) => {
    const [html, setHtml] = useState(null);
    const [value, setValue] = useState({});
    const [disabled, setDisabled] = useState(false);

    useEffect(() => {

        axios.get(api)
            .then(res => { setValue(res.data); setHtml(res.data?.html); });

    }, [api]);

    const handleEditorChange = content => {
        setHtml(content);
        setDisabled(true);
    };

    const handleSubmit = () => {

        axios.put(api, { html });

    };

    return (
        <div className="container mt-5">
            <div>
                <div className='mb-20 lg:mb-0'>
                    <div className="mt-10 sm:mt-0">
                        <div className="md:grid md:grid-cols-3 md:gap-6">
                            <div className="md:col-span-3 text-center">
                                <div className="px-4 sm:px-0">
                                    <h3 className="text-xl font-medium leading-6 text-gray-900">{name}</h3>
                                    <div className='w-14 h-1 rounded-full bg-blue-500 my-3 mx-auto'></div>
                                    <p className="mt-1 text-sm text-gray-600">Change your site {name} from here</p>
                                </div>
                            </div>
                            <div className="mt-5 md:mt-0 md:col-span-3 shadow-md">

                                <Editor
                                    apiKey='lvqt711cpeq01v698jhflbxp717997vgbw87fmao4npbptyt'
                                    init={{
                                        height: 700,
                                        plugins: [
                                            'advlist autolink lists link image charmap print preview anchor',
                                            'searchreplace visualblocks code fullscreen',
                                            'insertdatetime media table paste code preview wordcount'
                                        ],
                                        toolbar: 'undo redo |' +
                                            'bold italic backcolor forecolor | fontsizeselect | alignleft aligncenter ' +
                                            'alignright alignjustify | bullist numlist outdent indent'

                                    }}
                                    initialValue={value?.html}
                                    onEditorChange={handleEditorChange}
                                />

                                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">

                                    {

                                        disabled ? (

                                            <button
                                                onClick={handleSubmit}
                                                type="submit"
                                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                            >
                                                Save
                                            </button>

                                        ) : (

                                            <button
                                                disabled
                                                type="submit"
                                                className="opacity-20 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 cursor-not-allowed"
                                            >
                                                Save
                                            </button>

                                        )

                                    }

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div >

            {/* <div className="preview mt-5" dangerouslySetInnerHTML={{ __html: html }}></div> */}

        </div>
    );
};

export default FooterPageEditor;
