import React from 'react';
import { BiArrowBack } from 'react-icons/bi';
import { FaDownload } from 'react-icons/fa';
import { useNavigate, useParams } from 'react-router-dom';
import ReactToPrint from 'react-to-print';
import InvoiceTemplate from './InvoiceTemplate';

function InvoicePage() {
    const { id } = useParams();
    const navigate = useNavigate()
    const [orders, setOrders] = React.useState([])
    const componentRef = React.useRef();

    React.useEffect(() => {
        fetch(`https://api.powermall.com.bd/orders/${id}`)
            .then(res => res.json())
            .then(data => setOrders(data))
    }, [id])

    return (
        <div>
            <div className="px-4 py-5 sm:px-3 bg-gray-100 flex justify-between mb-4 rounded-lg">
                {/* heading  */}
                <h3 className="text-lg leading-6 font-medium text-gray-900 flex items-center space-x-3">
                    <BiArrowBack className='cursor-pointer' onClick={() => navigate('/dashboard/orders')} /> <span>Order ID - {orders?.orderID} </span>
                </h3>

                {/* download btn  */}
                <div>
                    <ReactToPrint
                        trigger={() => <button className="px-3 py-2 bg-blue-500 text-white outline-none border-none rounded-md text-xs cursor-pointer flex items-center space-x-2">
                            <FaDownload />
                            <span>Download/Print Invoice</span>
                        </button>}
                        content={() => componentRef.current}
                    />

                </div>
            </div>

            {/* invoice template  */}
            <InvoiceTemplate orders={orders} componentRef={componentRef} />
        </div>
    );
}

export default InvoicePage;