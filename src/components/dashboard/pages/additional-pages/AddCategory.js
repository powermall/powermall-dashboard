import axios from 'axios';
import React, { useRef, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { VscLoading } from 'react-icons/vsc';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

const AddCategory = () => {

    const [files, setFiles] = useState([]);
    const [categoryName, setCategoryName] = useState('');
    const [position, setPosition] = useState('');
    const subCategoryRef = useRef('');
    const [subCategoryNames, setSubCategoryNames] = useState([]);

    const { getRootProps, getInputProps } = useDropzone({
        accept: "image/*",
        onDrop: (acceptedFiles) => {
            setFiles(
                acceptedFiles.map((file) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    })
                )
            );
        },
    });

    const image = files.map((file) => (
        <div key={file.name}>
            <div>
                <img className='rounded-xl border-4 mt-10' src={file.preview} style={{ width: "100%", height: "160px", objectFit: "cover" }} alt="preview" />
                <h1 className='text-gray-600'>{file.name}</h1>
            </div>
        </div>
    ));

    const handleRemove = (removeText) => {

        const restItems = subCategoryNames.filter(item => item !== removeText);
        setSubCategoryNames(restItems);

    };

    const handleAddSubCategory = () => {

        const subCategoryName = subCategoryRef.current.value.toLowerCase();
        if (subCategoryName !== '') {
            setSubCategoryNames([...subCategoryNames, subCategoryName]);
            subCategoryRef.current.value = '';
        }

    };

    return (
        <div>
            <div>

                <div className="mt-10 sm:mt-0">
                    <div className="md:grid md:grid-cols-1 md:gap-6">
                        <div className="md:col-span-1 mt-5">
                            <div className="px-4 sm:px-0 text-center">
                                <h3 className="text-2xl font-medium leading-6 text-gray-900">Add Category</h3>
                                <div className='w-20 h-1 rounded-full bg-blue-600 mx-auto mt-5'></div>
                            </div>
                        </div>
                        <div className="mt-5 md:mt-0 md:col-span-2 lg:w-5/6 mx-auto">
                            <div>
                                <div className="shadow-xl overflow-hidden sm:rounded-md">
                                    <div className="px-4 py-5 bg-white sm:p-6">
                                        <div className="grid grid-cols-6 gap-6">

                                            <div className="col-span-6 sm:col-span-6">
                                                <div className="grid grid-cols-10 gap-x-5">
                                                    <div className='col-span-8'>
                                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                            Category Name
                                                        </label>
                                                        <input
                                                            required
                                                            onChange={(e) => setCategoryName(e.target.value)}
                                                            type="text"
                                                            name="first-name"
                                                            id="first-name"
                                                            className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                        />
                                                    </div>
                                                    <div className='col-span-2'>
                                                        <label>Position</label>
                                                        <input onChange={(e) => setPosition(e.target.value)} className="px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300" type="number" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-span-6 sm:col-span-6">

                                                <div className='w-full'>
                                                    <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                        Sub Category Name
                                                    </label>
                                                    <div className='flex items-center gap-x-4'>

                                                        <input
                                                            required
                                                            ref={subCategoryRef}
                                                            type="text"
                                                            name="first-name"
                                                            id="first-name"
                                                            className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                        />
                                                        <button onClick={handleAddSubCategory} className='justify-center py-3 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500' >Add</button>

                                                    </div>

                                                    <div className='flex gap-x-2 mt-4'>
                                                        {
                                                            subCategoryNames.map(_ => <div key={_} className='border border-blue-500 rounded-full py-1 pl-4 pr-7 flex items-center relative'>

                                                                <h1 className='capitalize'>{_}</h1>

                                                                <div onClick={() => handleRemove(_)} className='p-1 rounded-full cursor-pointer absolute right-0 hover:text-red-700' >

                                                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={1} d="M6 18L18 6M6 6l12 12" />
                                                                    </svg>

                                                                </div>

                                                            </div>)
                                                        }
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                    <div className="col-span-6">

                                        <div className="mt-5">

                                            <div className="sm:rounded-md sm:overflow-hidden">

                                                <div className="px-4 py-5 bg-white space-y-6 sm:p-6">

                                                    <div>
                                                        <label className="block text-sm font-medium text-gray-700">Category Image</label>
                                                        <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                                            <div className="space-y-1 text-center">
                                                                <svg
                                                                    className="mx-auto h-12 w-12 text-gray-400"
                                                                    stroke="currentColor"
                                                                    fill="none"
                                                                    viewBox="0 0 48 48"
                                                                    aria-hidden="true"
                                                                >
                                                                    <path
                                                                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                                                        strokeWidth={2}
                                                                        strokeLinecap="round"
                                                                        strokeLinejoin="round"
                                                                    />
                                                                </svg>
                                                                <div {...getRootProps()} className="flex flex-col lg:flex-row justify-center text-sm text-gray-600">
                                                                    <label
                                                                        htmlFor="file-upload"
                                                                        className="relative cursor-pointer bg-white rounded-md font-medium text-blue-600 hover:text-blue-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-blue-500"
                                                                    >
                                                                        <span>Upload an Image</span>
                                                                        <input {...getInputProps()} className="sr-only" />
                                                                    </label>
                                                                    <p className="pl-1">or drag and drop</p>
                                                                    <p></p>
                                                                </div>
                                                                <p className="text-xs text-gray-500">PNG, JPG up to 2MB</p>

                                                                <div className="uploaded__image flex gap-x-2">{image}</div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <BannerUpload category={categoryName} files1={files} setFiles1={setFiles} setCategoryName={setCategoryName} subCategoryNames={subCategoryNames} position={parseInt(position)} />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default AddCategory;


export const BannerUpload = ({ category, files1, setFiles1, setCategoryName, subCategoryNames, position }) => {

    const [files, setFiles] = useState([]);
    const [processing, setProcessing] = useState(false);
    const navigate = useNavigate();

    const { getRootProps, getInputProps } = useDropzone({
        accept: "image/*",
        onDrop: (acceptedFiles) => {
            setFiles(
                acceptedFiles.map((file) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    })
                )
            );
        },
    });

    const handleBannerUpload = () => {

        setProcessing(true);
        const formdata1 = new FormData();
        const formdata2 = new FormData();
        formdata1.append('image', files?.[0]);
        formdata2.append('image', files1?.[0]);

        if (files1.length === 1 && files.length === 1 && category.length > 1) {

            axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata1)
                .then(res => {

                    if (res.status === 200) {

                        const banner = res.data.data.image.url;

                        axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata2)
                            .then(res => {

                                if (res.status === 200) {

                                    const img = res.data.data.image.url;
                                    const slug = category.toLowerCase();
                                    const subCategory = subCategoryNames;
                                    const data = { category, slug, subCategory, img, banner, position };

                                    axios.post('https://api.powermall.com.bd/category', data)
                                        .then(res => {
                                            if (res.status === 200) {
                                                Swal.fire(
                                                    'Successful',
                                                    'Category Added Successfully',
                                                    'success'
                                                );

                                                setFiles([]);
                                                setFiles1([]);
                                                setCategoryName('');
                                                navigate('/dashboard/category');
                                            }
                                        });

                                }

                            });

                    }

                });

        } else if (files1.length === 1 && files.length === 0 && category.length > 1) {

            axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata2)
                .then(res => {

                    if (res.status === 200) {

                        const img = res.data.data.image.url;
                        const banner = '';
                        const slug = category.toLowerCase();
                        const subCategory = subCategoryNames;
                        const data = { category, slug, subCategory, img, banner, position };

                        axios.post('https://api.powermall.com.bd/category', data)
                            .then(res => {
                                if (res.status === 200) {
                                    Swal.fire(
                                        'Successful',
                                        'Category Added Successfully',
                                        'success'
                                    );

                                    setFiles([]);
                                    setCategoryName('');
                                    navigate('/dashboard/category');
                                }
                            });

                    }

                });

        } else if (files1.length === 0) {

            Swal.fire(
                'Error',
                'You Must Have To Upload 1 Image',
                'error'
            );

        } else if (category.length === 0) {

            Swal.fire(
                'Error',
                'You Must Have To Write Category Name',
                'error'
            );

        }

    };

    const image = files.map((file) => (
        <div key={file.name}>
            <div>
                <img className='rounded-xl border-4 mt-10' src={file.preview} style={{ width: "100%", height: "160px", objectFit: "cover" }} alt="preview" />
                <h1 className='text-gray-600'>{file.name}</h1>
            </div>
        </div>
    ));

    return (

        <div className="col-span-6">

            <div className="mt-5">

                <div className="sm:rounded-md sm:overflow-hidden">

                    <div className="px-4 py-5 bg-white space-y-6 sm:p-6">

                        <div>
                            <label className="block text-sm font-medium text-gray-700">Banner Image</label>
                            <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                <div className="space-y-1 text-center">
                                    <svg
                                        className="mx-auto h-12 w-12 text-gray-400"
                                        stroke="currentColor"
                                        fill="none"
                                        viewBox="0 0 48 48"
                                        aria-hidden="true"
                                    >
                                        <path
                                            d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                            strokeWidth={2}
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                        />
                                    </svg>
                                    <div {...getRootProps()} className="flex flex-col lg:flex-row justify-center text-sm text-gray-600">
                                        <label
                                            htmlFor="file-upload"
                                            className="relative cursor-pointer bg-white rounded-md font-medium text-blue-600 hover:text-blue-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-blue-500"
                                        >
                                            <span>Upload an Image</span>
                                            <input {...getInputProps()} className="sr-only" />
                                        </label>
                                        <p className="pl-1">or drag and drop</p>
                                        <p></p>
                                    </div>
                                    <p className="text-xs text-gray-500">PNG, JPG up to 2MB</p>

                                    <div className="uploaded__image flex gap-x-2">{image}</div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">

                        {

                            processing ? <button type="button" className="inline-flex gap-x-2 items-center justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 cursor-not-allowed" disabled>
                                <VscLoading className='animate-spin text-xl' />
                                Processing
                            </button> : <button
                                onClick={handleBannerUpload}
                                type="submit"
                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                            >
                                Save
                            </button>

                        }


                    </div>
                </div>
            </div>
        </div>

    );

};