import axios from 'axios';
import React, { useRef, useState } from 'react';
import { VscFile, VscNewFile } from 'react-icons/vsc';
import { CSVReader } from 'react-papaparse';
import Swal from 'sweetalert2';

const CSVupload = () => {

    const buttonRef = useRef(null);
    const [CSVdata, setCSVdata] = useState([]);
    const [disabled, setDisabled] = useState(false);

    const handleOnFieLoad = (data) => {

        console.log(data)
        const columns = data[0].data.map((col, index) => {

            return {
                header: col,
                accessor: col
            };

        });

        const rows = data?.slice(1).map(row => {
            return row.data.reduce((acc, curr, index) => {

                acc[columns[index]?.accessor] = curr;
                return acc;;

            }, {});
        });
        setCSVdata(rows);
    };

    const handleCSVupload = () => {

        console.log(CSVdata)
        axios.post('https://api.powermall.com.bd/products', CSVdata)
            .then(res => {
                if (res.status === 200) {
                    Swal.fire('Successful', 'CSV File Successfully Uploaded', 'success');
                    setDisabled(false);
                    handleRemove();
                }
            });

    };

    const handleOpenCSVReader = (e) => {
        if (buttonRef.current) {
            buttonRef.current.open(e);
        }
    };

    const handleRemove = (e) => {
        if (buttonRef.current) {
            buttonRef.current.removeFile(e);
        }
    };


    return (
        <div>
            <div className="my-20 border rounded-xl shadow-xl">

                <div className="sm:rounded-md sm:overflow-hidden p-1">
                    <div className="px-4 py-5 bg-white space-y-6 sm:p-6">

                        <div onChange={() => setDisabled(true)}>
                            <label className="block font-medium text-gray-700 text-center text-xl">CSV File Upload</label>
                            <div className='w-10 h-1 mt-2 mb-5 mx-auto rounded-full bg-blue-500'></div>

                            <CSVReader
                                ref={buttonRef}
                                onFileLoad={handleOnFieLoad}
                                onClick
                            >

                                {
                                    ({ file }) => (
                                        <>
                                            <div className='flex flex-col w-full mx-auto p-3' onClick={handleOpenCSVReader}>
                                                <label className="custom-file-upload">

                                                    <input type="file" accept=".csv" />
                                                    {
                                                        !file && <div className='flex-col flex items-center justify-center gap-x-4 my-5'>
                                                            <VscNewFile className='text-5xl text-gray-400 mb-2' />
                                                            <h1 className='text-sm text-blue-500 font-semibold'>Upload File</h1>
                                                        </div>
                                                    }

                                                    <div className='flex items-center justify-center gap-x-4 my-5'>

                                                        {file &&
                                                            <>
                                                                <div className='flex items-center gap-x-2'>
                                                                    <VscFile className='text-3xl text-gray-400 mb-2' />
                                                                    <h1 className='text-blue-500'>{file.name}</h1>
                                                                </div>

                                                                <button onClick={handleRemove} className='border px-2 rounded border-gray-400'>Remove</button>
                                                            </>
                                                        }
                                                    </div>
                                                </label>
                                            </div>
                                            <div className='text-right mx-4 text-blue-600'>
                                                <a href="/Products.csv" download className='text-center text-sm hover:underline'>Demo CSV Download</a>
                                            </div>
                                        </>
                                    )
                                }

                            </CSVReader>

                        </div>
                    </div>
                </div>

                <div className='p-4 flex justify-end rounded-b-lg bg-gray-50'>
                    {

                        disabled ? (

                            <button
                                onClick={handleCSVupload}
                                type="submit"
                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                            >
                                Save
                            </button>

                        ) : (

                            <button
                                disabled
                                type="submit"
                                className="opacity-20 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 cursor-not-allowed"
                            >
                                Save
                            </button>

                        )

                    }
                </div>

            </div>
        </div>
    );
};

export default CSVupload;
