import { Dialog, Transition } from '@headlessui/react';
import React, { Fragment, useRef } from 'react';

const ProductModal = ({ open, setOpen, product }) => {
    const cancelButtonRef = useRef(null);

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog as="div" className="fixed z-10 inset-0 overflow-y-auto" initialFocus={cancelButtonRef} onClose={setOpen}>
                <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Dialog.Overlay className="fixed inset-0 bg-gray-100 bg-opacity-20 transition-opacity" />
                    </Transition.Child>

                    {/* This element is to trick the browser into centering the modal contents. */}
                    <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
                        &#8203;
                    </span>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        enterTo="opacity-100 translate-y-0 sm:scale-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                        leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"

                    >
                        <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden border-2 transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full " style={{ width: '500px' }}>
                            <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div className="sm:flex sm:items-start">
                                    <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                        <div className='border-b border-gray-300 pb-4 w-full'>
                                            <Dialog.Title as="h3" className="text-lg leading-6  font-medium text-gray-600 ">
                                                Ordered Products ({product?.cart?.length} items)
                                            </Dialog.Title>
                                        </div>

                                        <div className="w-full" style={{ width: '430px' }}>
                                            {product?.cart?.map(item => {
                                                const image = item?.img?.split(',');
                                                const newPrice = parseInt(item?.price);

                                                let discountPrice;

                                                if(item?.discount){
                                                    discountPrice = parseInt(item?.discount);
                                                }else{
                                                    discountPrice = 0
                                                }

                                                return (
                                                    <div className='my-3 mx-1 rounded-lg border border-gray-200' key={item?._id}>
                                                        <div className="px-3 py-4 flex justify-between space-x-1 bg-white">
                                                            {/* image  */}
                                                            <div>
                                                                <img src={image?.[0]} alt={item?._id} className='object-contain w-16' />
                                                            </div>

                                                            {/* infos  */}
                                                            <div className="flex flex-col space-y-2">
                                                                <h1 className='break-all text-sm'>{item?.name}</h1>
                                                                <p className='text-primary'>&#2547; {newPrice -  discountPrice} <span className='text-gray-600'>({item?.pdQuantity} pcs)</span></p>
                                                                <h1 className='text-sm'><span className='font-medium'>Color :</span> {item?.pdColor}</h1>
                                                            </div>

                                                            {/* price  */}
                                                            <div className='flex flex-col justify-center'>
                                                                <h2 className='text-gray-700'>&#2547; {(newPrice - discountPrice) * item?.pdQuantity}</h2>
                                                            </div>


                                                        </div>
                                                    </div>
                                                );
                                            })}
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition.Root>
    );
};

export default ProductModal;