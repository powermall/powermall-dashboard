import axios from 'axios';
import React, { useRef, useState } from 'react';
import Swal from 'sweetalert2';
import DatePicker1, { DatePicker2 } from './DatePicker';

const AddCoupons = () => {

    const [couponStart, setCouponStart] = useState(new Date());
    const [couponEnd, setCouponEnd] = useState(new Date());
    const codeRef = useRef('');
    const ammountRef = useRef('');

    const handleSubmit = (e) => {
        e.preventDefault();

        const code = codeRef.current.value;
        const ammount = ammountRef.current.value;
        const start = couponStart.toLocaleDateString();
        const end = couponEnd.toLocaleDateString();
        const data = { code, ammount, start, end };

        axios.post('https://api.powermall.com.bd/coupons', data)
            .then(res => {
                e.target.reset();
                setCouponStart(new Date());
                setCouponEnd(new Date());
                Swal.fire(
                    'Successful',
                    'Coupon Added Successfully',
                    'success'
                );
            });

    };

    return (
        <div>
            <div className="mt-10 sm:mt-0">
                <div className="md:grid md:grid-cols-1 md:gap-6">
                    <div className="md:col-span-1 mt-5">
                        <div className="px-4 sm:px-0 text-center">
                            <h3 className="text-2xl font-medium leading-6 text-gray-900">Add Coupons</h3>
                            <div className='w-20 h-1 rounded-full bg-blue-600 mx-auto mt-5'></div>
                        </div>
                    </div>
                    <div className="mt-5 md:mt-0 md:col-span-2 w-5/6 mx-auto">
                        <form onSubmit={handleSubmit}>
                            <div className="shadow-xl overflow-hidden sm:rounded-md">
                                <div className="px-4 py-5 grid grid-cols-12 gap-6 bg-white sm:p-6">
                                    <div className="col-span-6">
                                        <div className="col-span-6 sm:col-span-12">
                                            <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                Coupons Code
                                            </label>
                                            <input
                                                required
                                                ref={codeRef}
                                                type="text"
                                                name="first-name"
                                                id="first-name"
                                                className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                    </div>
                                    <div className="col-span-6">
                                        <div className="col-span-6 sm:col-span-12">
                                            <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                Amount
                                            </label>
                                            <input
                                                required
                                                ref={ammountRef}
                                                type="number"
                                                name="first-name"
                                                id="first-name"
                                                className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>
                                    </div>


                                </div>

                                <div className='grid grid-cols-2 mb-10'>

                                    <div>
                                        <h1 className='text-center mb-2 font-medium'>Active Form</h1>
                                        <div className='h-1 w-14 rounded-full mx-auto bg-blue-600 mb-3'></div>
                                        <div className='w-3/6 mx-auto rounded-xl p-1 border'>
                                            <DatePicker1 couponStart={couponStart} setCouponStart={setCouponStart} />
                                        </div>
                                    </div>
                                    <div>
                                        <h1 className='text-center mb-2 font-medium'>Will Expire</h1>
                                        <div className='h-1 w-14 rounded-full mx-auto bg-blue-600 mb-3'></div>
                                        <div className='w-3/6 mx-auto rounded-xl p-1 border'>
                                            <DatePicker2 couponEnd={couponEnd} setCouponEnd={setCouponEnd} />
                                        </div>
                                    </div>

                                </div>

                                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    <button
                                        type="submit"
                                        className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                    >
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AddCoupons;
