import { Editor } from '@tinymce/tinymce-react';
import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import Swal from 'sweetalert2';
import CSVupload from './CSVupload';

const AddProducts = () => {
    const [html, setHtml] = useState(null);
    const [html2, setHtml2] = useState(null);
    const [files, setFiles] = useState([]);
    const [brands, setBrands] = useState([]);
    const [categories, setCategories] = useState([]);
    const [subCategory, setSubCategory] = useState([]);
    const [grid, setGrid] = useState(false);
    const nameRef = useRef('');
    const priceRef = useRef('');
    const discountRef = useRef('');
    // const moreInformationRef = useRef('');
    const warentyRef = useRef('');
    const quantityRef = useRef('');
    // const descriptionRef = useRef('');
    const ytLinkRef = useRef('');
    const skuRef = useRef('');
    const metaRef = useRef('');
    const colorRef = useRef('');
    const brandRef = useRef('');
    const statusRef = useRef('');
    const categoryRef = useRef('');
    const subCategoryRef = useRef('');
    const deliveryRef = useRef('');
    const deliveryOptionRef = useRef('');
    const storePickupRef = useRef('');
    const shippingChargeRef = useRef('');

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/brands')
            .then(res => setBrands(res.data));

        axios.get('https://api.powermall.com.bd/category')
            .then(res => setCategories(res.data));

    }, []);

    const handleEditorChange = content => {
        setHtml(content);
    };
    const handleEditorChange2 = content => {
        setHtml2(content);
    };

    const { getRootProps, getInputProps } = useDropzone({
        accept: "image/*",
        onDrop: (acceptedFiles) => {
            setFiles(
                acceptedFiles?.map((file) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    })
                )
            );
        },
    });

    const image = files.map((file) => (
        <div key={file.name}>
            <div>
                <img className='rounded-xl border-4 mt-10' src={file.preview} style={{ width: "100%", height: "160px", objectFit: "cover" }} alt="preview" />
                <h1 className='text-gray-600'>{file.name}</h1>
            </div>
        </div>
    ));

    const handleSubCategory = (e) => {

        const subCategoryName = e.target.value;
        axios.get(`https://api.powermall.com.bd/category/${subCategoryName}`)
            .then(res => setSubCategory(res.data.subCategory));

    };

    useEffect(() => {

        if (subCategory?.length !== 0) {
            setGrid(true);
        } else {
            setGrid(false);
        }

    }, [subCategory]);

    const postImg = (e) => {
        e.preventDefault();

        const formdata1 = new FormData();
        const formdata2 = new FormData();
        const formdata3 = new FormData();
        const formdata4 = new FormData();
        formdata1.append('image', files?.[0]);
        formdata2.append('image', files?.[1]);
        formdata3.append('image', files?.[2]);
        formdata4.append('image', files?.[3]);
        let allImg = [];

        if (files) {

            axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata1)
            .then((response) => {
                if (response.status === 200) {
                    const img4 = response.data.data.image.url;
                    allImg.push(img4);
                    handleAddProduct(allImg);
                    e.target.reset();
                    setFiles([]);
                    Swal.fire(
                        'Successful',
                        'Product Added Successfully',
                        'success'
                    );
                }
            })

            axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata2)
            .then((response) => {
                if (response.status === 200) {
                    const img4 = response.data.data.image.url;
                    allImg.push(img4);
                    handleAddProduct(allImg);
                    e.target.reset();
                    setFiles([]);
                    Swal.fire(
                        'Successful',
                        'Product Added Successfully',
                        'success'
                    );
                }
            })

            axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata3)
                .then((response) => {
                            if (response.status === 200) {
                                const img4 = response.data.data.image.url;
                                allImg.push(img4);
                                handleAddProduct(allImg);
                                e.target.reset();
                                setFiles([]);
                                Swal.fire(
                                    'Successful',
                                    'Product Added Successfully',
                                    'success'
                                );
                            }
                        })


            setTimeout(() => {

                axios.post('https://api.imgbb.com/1/upload?key=a1a59ec813f7ab9889dd822f6f1ceaba', formdata4)

                    .then((response) => {
                        if (response.status === 200) {
                            const img4 = response.data.data.image.url;
                            allImg.push(img4);
                            handleAddProduct(allImg);
                            e.target.reset();
                            setFiles([]);
                            Swal.fire(
                                'Successful',
                                'Product Added Successfully',
                                'success'
                            );
                        }
                    })

                    .catch((error) => {
                        Swal.fire(
                            'Error',
                            'Reload The Page And Try Again',
                            'error'
                        );
                    });

            }, 1000);

        } else if (files.length > 4) {
            Swal.fire(
                'Error',
                'You Can Not Upload More Then 4 Images',
                'error'
            );
        }   
        else {
            Swal.fire(
                'Error',
                'Plz Upload Image',
                'error'
            );
        }

    };

    const handleAddProduct = (allImg) => {

        const name = nameRef.current.value;
        const price = priceRef.current.value;
        const discount = discountRef.current.value;
        const warenty = warentyRef.current.value;
        const quantity = quantityRef.current.value;
        // const description = descriptionRef.current.value;
        // const moreInformation = moreInformationRef.current.value;
        const ytLink = ytLinkRef.current.value;
        const sku = skuRef.current.value;
        const meta = metaRef.current.value;
        const color = colorRef.current.value;
        const brand = brandRef.current.value;
        const category = categoryRef.current.value;
        const delivery = deliveryRef.current.value;
        const status = statusRef.current.value;
        const deliveryoption = deliveryOptionRef.current.value;
        const storePickup = storePickupRef.current.value;
        const shippingCharge = shippingChargeRef.current.value;

        let subCategory = subCategoryRef.current.value;
        const time = new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
        const formData = new FormData();

        if (subCategory === 'Select Sub Category') {
            subCategory = '';
        }

        formData.append('name', name);
        formData.append('price', price);
        formData.append('discount', discount);
        formData.append('warenty', warenty);
        formData.append('quantity', quantity);
        formData.append('description', html);
        formData.append('moreInformation', html2);
        formData.append('ytLink', ytLink);
        formData.append('sku', sku);
        formData.append('meta', meta);
        formData.append('color', color);
        formData.append('brand', brand);
        formData.append('category', category);
        formData.append('subCategory', subCategory);
        formData.append('delivery', delivery);
        formData.append('status', status);
        formData.append('time', time);
        formData.append('deliveryOption', deliveryoption);
        formData.append('storePickup', storePickup);
        formData.append('shippingCharge', shippingCharge);
        formData.append('img', allImg);

        axios.post('https://api.powermall.com.bd/products', formData);

    };

    return (
        <div>
            <div>

                <div className="mt-10 sm:mt-0">
                    <div className="md:grid md:grid-cols-1 md:gap-6">
                        <div className="md:col-span-1 mt-5">
                            <div className="px-4 sm:px-0 text-center">
                                <h3 className="text-2xl font-medium leading-6 text-gray-900">Add Products</h3>
                                <div className='w-20 h-1 rounded-full bg-blue-600 mx-auto mt-5'></div>
                            </div>
                        </div>
                        <div className="mt-5 md:mt-0 md:col-span-2 lg:w-5/6 mx-auto">
                            <form onSubmit={postImg}>
                                <div className="shadow-xl overflow-hidden sm:rounded-md">
                                    <div className="px-4 py-5 bg-white sm:p-6">
                                        <div className="grid grid-cols-6 gap-6">
                                            <div className="col-span-6 sm:col-span-3">
                                                <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                    Name
                                                </label>
                                                <input
                                                    required
                                                    ref={nameRef}
                                                    type="text"
                                                    name="first-name"
                                                    id="first-name"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-3">
                                                <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                                                    Price
                                                </label>
                                                <input
                                                    required
                                                    ref={priceRef}
                                                    type="number"
                                                    min='0'
                                                    name="last-name"
                                                    id="last-name"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-6">
                                                <label htmlFor="email-address" className="block text-sm font-medium text-gray-700">
                                                    Specification
                                                </label>
                                                {/* <textarea
                                                    required
                                                    ref={descriptionRef}
                                                    type="text"
                                                    name="email-address"
                                                    id="email-address"
                                                    className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                /> */}
                                                <Editor
                                                    apiKey='lvqt711cpeq01v698jhflbxp717997vgbw87fmao4npbptyt'
                                                    init={{
                                                        height: 400,
                                                        plugins: [
                                                            'advlist autolink lists link image charmap print preview anchor',
                                                            'searchreplace visualblocks code fullscreen',
                                                            'insertdatetime media table paste code preview wordcount'
                                                        ],
                                                        toolbar: 'undo redo |' +
                                                            'bold italic backcolor forecolor | fontsizeselect | alignleft aligncenter ' +
                                                            'alignright alignjustify | bullist numlist outdent indent'

                                                    }}
                                                    // initialValue={value?.html}
                                                    onEditorChange={handleEditorChange}
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-6">
                                                <label htmlFor="email-address" className="block text-sm font-medium text-gray-700">
                                                   Details
                                                </label>
                                                <Editor
                                                    apiKey='lvqt711cpeq01v698jhflbxp717997vgbw87fmao4npbptyt'
                                                    init={{
                                                        height: 400,
                                                        plugins: [
                                                            'advlist autolink lists link image charmap print preview anchor',
                                                            'searchreplace visualblocks code fullscreen',
                                                            'insertdatetime media table paste code preview wordcount'
                                                        ],
                                                        toolbar: 'undo redo |' +
                                                            'bold italic backcolor forecolor | fontsizeselect | alignleft aligncenter ' +
                                                            'alignright alignjustify | bullist numlist outdent indent'

                                                    }}
                                                    // initialValue={value?.html}
                                                    onEditorChange={handleEditorChange2}
                                                />
                                            </div>

                                            <div className="col-span-6">
                                                <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                                    Youtube Video Link
                                                </label>
                                                <input
                                                    ref={ytLinkRef}
                                                    type="text"
                                                    name="street-address"
                                                    id="street-address"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                            <div className="col-span-3">
                                                <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                                    SKU
                                                </label>
                                                <input
                                                    required
                                                    ref={skuRef}
                                                    type="text"
                                                    name="street-address"
                                                    id="street-address"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                            <div className="col-span-3">
                                                <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                                    Meta
                                                </label>
                                                <input
                                                    required
                                                    ref={metaRef}
                                                    type="text"
                                                    name="street-address"
                                                    id="street-address"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                            <div className="col-span-2">
                                                <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                                    Brand
                                                </label>
                                                <select
                                                    ref={brandRef}
                                                    id="country"
                                                    name="country"
                                                    autoComplete="country-name"
                                                    className="mt-1 block w-full py-3 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
                                                >
                                                    <option>Please Select A Brand</option>
                                                    {
                                                        brands?.map(brand => <option key={brand._id}>{brand?.brand}</option>)
                                                    }

                                                </select>
                                            </div>

                                            <div className="col-span-2">
                                                <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                                    Special Category
                                                </label>
                                                <select
                                                    ref={deliveryRef}
                                                    id="country"
                                                    name="country"
                                                    autoComplete="country-name"
                                                    className="mt-1 block w-full py-3 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
                                                >
                                                    {
                                                        ['Paid Shipping', 'Free Shipping', 'Drone Service', 'Global Purchase', 'Mobile', 'Upcoming', 'Cash On Delivery', 'Whole Sale Vault','Single Purchase'].map(option => <option>{option}</option>)
                                                    }

                                                </select>
                                            </div>

                                            <div className="col-span-2">
                                                <div className={`grid gap-x-5 ${grid ? 'grid-cols-2' : 'grid-cols-1'}`}>

                                                    <div>
                                                        <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                                            Category
                                                        </label>
                                                        <select
                                                            ref={categoryRef}
                                                            onChange={handleSubCategory}
                                                            id="country"
                                                            name="country"
                                                            autoComplete="country-name"
                                                            className="mt-1 block w-full py-3 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
                                                        >
                                                            <option>Select A Category</option>
                                                            {
                                                                categories?.map(category => <option>{category?.category}</option>)
                                                            }

                                                        </select>
                                                    </div>

                                                    <div>
                                                        {
                                                            subCategory?.length !== 0 && (

                                                                <div>

                                                                    <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                                                        Sub Category
                                                                    </label>

                                                                    <select
                                                                        ref={subCategoryRef}
                                                                        id="country"
                                                                        name="country"
                                                                        autoComplete="country-name"
                                                                        className="mt-1 block w-full py-3 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
                                                                    >
                                                                        <option>Select Sub Category</option>
                                                                        {
                                                                            subCategory?.map(category => <option>{category}</option>)
                                                                        }

                                                                    </select>

                                                                </div>

                                                            )
                                                        }
                                                    </div>
                                                </div>
                                            </div>


                                            <div className="col-span-6 sm:col-span-6 lg:col-span-3">
                                                <label htmlFor="city" className="block text-sm font-medium text-gray-700">
                                                    Discount (Amount)
                                                </label>
                                                <input
                                                    required
                                                    defaultValue={0}
                                                    ref={discountRef}
                                                    type="number"
                                                    min="0"
                                                    name="city"
                                                    id="city"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                            <div className="col-span-3">
                                                <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                                    Warenty
                                                </label>
                                                <select
                                                    ref={warentyRef}
                                                    id="country"
                                                    name="country"
                                                    autoComplete="country-name"
                                                    className="mt-1 block w-full py-3 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
                                                >
                                                    <option>Available</option>
                                                    <option>Unavailable</option>

                                                </select>
                                            </div>

                                            <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                                                <label htmlFor="city" className="block text-sm font-medium text-gray-700">
                                                    Color
                                                </label>
                                                <input
                                                    required
                                                    ref={colorRef}
                                                    type="text"
                                                    name="city"
                                                    id="city"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                                                <label htmlFor="region" className="block text-sm font-medium text-gray-700">
                                                    Quantity
                                                </label>
                                                <input
                                                    required
                                                    ref={quantityRef}
                                                    defaultValue={1}
                                                    type="number"
                                                    min='1'
                                                    name="region"
                                                    id="region"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                                                <label htmlFor="region" className="block text-sm font-medium text-gray-700">
                                                    Status
                                                </label>
                                                <select
                                                    ref={statusRef}
                                                    id="country"
                                                    name="country"
                                                    autoComplete="country-name"
                                                    className="mt-1 block w-full py-3 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
                                                >
                                                    <option>Available</option>
                                                    <option>Unavailable</option>
                                                </select>
                                            </div>

                                            <div className="col-span-6 sm:col-span-6 lg:col-span-2">
                                                <label htmlFor="city" className="block text-sm font-medium text-gray-700">
                                                    Delivery Options
                                                </label>
                                                <input
                                                    required
                                                    ref={deliveryOptionRef}
                                                    type="text"
                                                    min="0"
                                                    name="city"
                                                    id="city"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-6 lg:col-span-2">
                                                <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                                    Store Pickup
                                                </label>
                                                <input
                                                    required
                                                    ref={storePickupRef}
                                                    type="text"
                                                    min="0"
                                                    name="city"
                                                    id="city"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>
                                            <div className="col-span-6 sm:col-span-6 lg:col-span-2">
                                                <label htmlFor="street-address" className="block text-sm font-medium text-gray-700">
                                                    Shipping Charge
                                                </label>
                                                <input
                                                    required
                                                    ref={shippingChargeRef}
                                                    type="number"
                                                    min="0"
                                                    name="city"
                                                    id="city"
                                                    className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                                />
                                            </div>

                                            <div className="col-span-6">

                                                <div className="mt-5">

                                                    <div className="sm:rounded-md sm:overflow-hidden">
                                                        <div className="px-4 py-5 bg-white space-y-6 sm:p-6">

                                                            <div>
                                                                <label className="block text-sm font-medium text-gray-700">Cover photo</label>
                                                                <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                                                    <div className="space-y-1 text-center">
                                                                        <svg
                                                                            className="mx-auto h-12 w-12 text-gray-400"
                                                                            stroke="currentColor"
                                                                            fill="none"
                                                                            viewBox="0 0 48 48"
                                                                            aria-hidden="true"
                                                                        >
                                                                            <path
                                                                                d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                                                                strokeWidth={2}
                                                                                strokeLinecap="round"
                                                                                strokeLinejoin="round"
                                                                            />
                                                                        </svg>
                                                                        <div {...getRootProps()} className="flex flex-col lg:flex-row justify-center text-sm text-gray-600">
                                                                            <label
                                                                                htmlFor="file-upload"
                                                                                className="relative cursor-pointer bg-white rounded-md font-medium text-blue-600 hover:text-blue-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-blue-500"
                                                                            >
                                                                                <span>Upload an Image</span>
                                                                                <input {...getInputProps()} className="sr-only" />
                                                                            </label>
                                                                            <p className="pl-1">or drag and drop</p>
                                                                        </div>
                                                                        <p className="text-xs text-gray-500">PNG, JPG up to 2MB</p>

                                                                        <div className="uploaded__image flex gap-x-2">{image}</div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                        <button
                                            type="submit"
                                            className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                        >
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

            <div className='w-5/6 mx-auto'>
                <CSVupload />
            </div>

        </div>
    );
};

export default AddProducts;
