import axios from 'axios';
import React, { useEffect, useState } from 'react';
// import Brand from '../Navbar/Brand';

function InvoiceTemplate({ orders, componentRef }) {
    const [address, setAddress] = useState()
 const [logo,setLogo] = useState();
    useEffect(() => {
        axios.get('https://api.powermall.com.bd/logo')
            .then(res => setLogo(res.data[0]))
    },[]);

    useEffect(() => {
        axios.get('https://api.powermall.com.bd/information')
            .then(res => setAddress(res.data[0]))
    }, [])


    return (
        // <React.forwardRef>
        <div className='bg-white rounded-lg p-8' ref={componentRef}>

            {/* top header  */}
            <div className='flex justify-between items-center'>

                {/* brand  */}
                <div>
                <div>
                    <img src={logo?.logo} alt={logo?._id} className="w-60 h-12 object-contain cursor-pointer" />
               </div>

                    {/* address  */}
                    <div className='w-44'>
                        <span className='text-xs text-gray-600'>{address?.address}</span>
                    </div>
                </div>

                {/*  and info  */}

                <div>
                    <h1 className='text-3xl font-bold'>INVOICE</h1>

                    <div className='flex flex-col text-xs text-gray-700 mt-2'>
                        <span>Order ID : {orders?.orderID}</span>
                        {orders?.paymentType === "Pre Payment" && (
                            <span>Transaction ID : {orders?.epw_txnid}</span>
                        )}
                        <span>Date: {orders?.date},{orders?.time} </span>
                    </div>
                </div>
            </div>


            {/* bill to  */}
            <div className="mt-6 ">
                <div>
                    <h1 className="text-sm font-semibold">Bill To</h1>

                    {/* customer name and mobile  */}
                    <div className='flex flex-col text-xs text-gray-700 mt-2'>
                        <span>{orders?.name}</span>
                        <span>Mobile : {orders?.mobile} </span>
                    </div>
                </div>

                {/* delivery address  */}
                <div className="mt-4">
                    <h1 className="text-sm font-semibold">Delivery Address</h1>

                    {/* customer name and mobile  */}
                    <div className='flex flex-col text-xs text-gray-700 mt-2'>
                        <span>Mobile : {orders?.mobile} </span>
                        <span>Email : {orders?.email} </span>
                        <span> {orders?.address} </span>
                    </div>
                </div>

            </div>



            {/* products table  */}
            <div className="flex flex-col">
                <div className="my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div className=" overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table className="min-w-full divide-y divide-gray-200">
                                <thead className="">
                                    <tr>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Image & Name
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Quantity
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Amount
                                        </th>

                                    </tr>
                                </thead>
                                <tbody className="bg-white divide-y divide-gray-200">
                                    {orders?.cart?.map((item) => {
                                        const image = item?.img?.split(",");
                                        // const price = (parseFloat(item.price) - parseFloat(item.discount) * parseFloat(item.discount)/100) * item.pdQuantity;
                                        const newPrice = parseInt(item?.price);
                                        let discountPrice;
                                        if (item?.discount) {
                                            discountPrice = parseInt(item?.discount);
                                        } else {
                                            discountPrice = 0
                                        }
                                        return (
                                            <tr key={item._id}>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <div className="flex items-center">
                                                        <div className="flex-shrink-0 h-10 w-10">
                                                            <img
                                                                className="h-10 w-10 object-contain"
                                                                src={image[0]}
                                                                alt={item.name}
                                                            />
                                                        </div>
                                                        <div className="ml-4">
                                                            <div className="text-xs font-medium text-gray-900 w-64 overflow-hidden">
                                                                {item.name}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <span className="px-2 text-xs leading-5 font-semibold text-gray-600 flex justify-center">
                                                        {item?.pdQuantity}
                                                    </span>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <span className="text-xs font-semibold text-gray-900 flex justify-center">
                                                        &#2547;{" "}
                                                        {(newPrice - discountPrice) *
                                                            item.pdQuantity}
                                                    </span>
                                                </td>



                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>




            {/* total  */}
            <div className="flex justify-end items-center my-6">

                {/* paid img  */}
                {/* <div className="w-56">
                    <img className="w-full h-full object-contain" src="../../../assets/Screenshot_1.png" alt="paid" />
                </div> */}

                {/* total  */}

                <div className="flex flex-col justify-end space-y-3">
                    {/* total  */}
                    <div className="flex items-center justify-between w-44 text-sm">
                        <h2>Total</h2>
                        <span className="font-bold">Tk. {parseInt(orders?.price)}</span>
                    </div>
                    {/* paid  */}
                    <div className="flex items-center justify-between w-44 text-sm">
                        <h2>Paid</h2>
                        <span className="font-bold">
                            {orders?.paymentType === "Pre Payment" ? `Tk. ${parseInt(orders?.price)}` : "Tk. 0" }
                        </span>
                    </div>
                    {/* due  */}
                    <div className="flex items-center justify-between w-44 text-sm">
                        <h2>Due</h2>
                        <span className="font-bold"> {orders?.paymentType !== "Pre Payment" ? `Tk. ${parseInt(orders?.price)}` : "Tk. 0"}</span>
                    </div>
                </div>

            </div>


        </div>
        // </React.forwardRef>
    );
}

export default InvoiceTemplate;