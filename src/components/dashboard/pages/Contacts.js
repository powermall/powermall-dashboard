import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiOutlineDelete } from 'react-icons/ai';
import Swal from 'sweetalert2';
import MessageModal from './additional-pages/MessageModal';

const Contacts = () => {

    const [messages, setMessages] = useState([]);
    const [open, setOpen] = useState(false);
    const [text, setText] = useState('');


    useEffect(() => {

        axios.get('https://api.powermall.com.bd/messages')
            .then(res => setMessages(res.data));

    }, [messages]);


    const handleModal = (_id) => {


        axios.get(`https://api.powermall.com.bd/messages/${_id}`)
            .then(res => {
                if (res.status === 200) {
                    setText(res.data);
                    setOpen(true);
                }
            });

    };

    const handleDelete = (_id) => {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                axios.delete(`https://api.powermall.com.bd/messages/${_id}`)
                    .then(res => {
                        if (res.data.deletedCount > 0) {
                            Swal.fire(
                                'Successful',
                                'Contact Deleted Successfully',
                                'success'
                            );
                        }
                    });
            }
        });

    };

    return (
        <div>

            <div>

                <div>
                    <h1 className='text-center text-2xl mb-2'>Messages</h1>
                    <div className='h-1 w-14 mx-auto bg-blue-600 rounded-full'></div>
                </div>

                <div className="flex flex-col shadow-2xl mt-10">
                    <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-6">
                        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-6">
                            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table className="min-w-full divide-y divide-gray-200">
                                    <thead className="bg-gray-50">
                                        <tr >
                                            <th
                                                scope="col"
                                                className="px-3 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Name
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Subject
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Email
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Message
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Actions
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody className="bg-white divide-y divide-gray-200">

                                        {messages.length === 0 ? <tr>

                                            <td colSpan={12} className='py-3'>

                                                <h1 className='text-gray-400 text-sm font-medium text-center'>No Data Found</h1>

                                            </td>

                                        </tr> : ''}

                                        {
                                            messages?.map((message) => {

                                                return (
                                                    <>
                                                        <tr key={message._id} className='text-center hover:bg-gray-100'>

                                                            <td className="px-6 py-1 whitespace-nowrap">
                                                                <h1 title={message?.name} className='truncate w-48 mx-auto'>{message?.name}</h1>
                                                            </td>

                                                            <td className="px-6 py-1 whitespace-nowrap">
                                                                <h1>{message?.subject}</h1>
                                                            </td>

                                                            <td className="px-6 py-1 whitespace-nowrap">
                                                                <h1>{message?.email}</h1>
                                                            </td>

                                                            <td className="px-6 py-1 whitespace-nowrap">
                                                                <h1 className='w-52 mx-auto truncate'>{message?.message}</h1>
                                                            </td>

                                                            <td className="px-6 py-1 whitespace-nowrap">
                                                                <div className='flex gap-x-4 justify-center'>

                                                                    <button onClick={() => handleModal(message?._id)} className='px-4 text-sm rounded-md border bg-blue-500 hover:bg-blue-700 text-white transition duration-500 shadow-lg'>View</button>
                                                                    <button onClick={() => handleDelete(message?._id)} className='p-2 border rounded-full bg-red-500  hover:bg-red-700 text-white transition duration-500 shadow-lg'><AiOutlineDelete className='text-xl' /></button>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <MessageModal open={open} setOpen={setOpen} text={text} />
                                                    </>
                                                );

                                            })

                                        }

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div >
    );
};

export default Contacts;

