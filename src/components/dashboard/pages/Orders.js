import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiOutlineDelete } from 'react-icons/ai';
import { BsCheck2, BsSearch } from 'react-icons/bs';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import AddressModal from './additional-pages/AddressModal';
import ProductModal from './additional-pages/ProductModal';

const Orders = () => {
    const [pd, setPd] = useState([]);
    const [open, setOpen] = useState(false);
    const [orders, setOrders] = useState([]);
    const [product, setProduct] = useState({});
    const [rerender, setRerender] = useState(0);
    const [addressOpen, setAddressOpen] = useState(false);
    const [address1,setAddress1] = useState()
    // const [order, setOrder] = useState({});              
    const navigate = useNavigate();

    useEffect(() => {

        axios.get(`https://api.powermall.com.bd/orders`)
            .then(res => {
                setPd(res.data?.map(product => product));
                setOrders(res.data);
            });

    }, [rerender]);

    const handleModal = (id) => {
        const productFind = pd?.find(product => product._id === id);
        setOpen(true);
        setProduct(productFind);
    };

    const handleAddress = (id) => {
        const addressFind = pd?.find(product => product._id === id);
        setAddressOpen(true);
        setAddress1(addressFind?.address)
    }
    // const handleInvoice = (id) => {
    //     const orderFind = orders?.find(order => order._id === id);
    //     setOpen(true);
    //     setOrder(orderFind);
    // }

    const handleDelete = (_id) => {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                axios.delete(`https://api.powermall.com.bd/orders/${_id}`)
                    .then(res => {
                        if (res.data.deletedCount > 0) {
                            const newOrder = orders.filter(order => order?._id !== _id);
                            setOrders(newOrder);
                            setPd(newOrder);

                            Swal.fire(
                                'Deleted!',
                                'Order has been deleted.',
                                'success'
                            );

                        }
                    });
            }
        });

    };

    const sendToOrderStatus = (_id) => {

        axios.put(`https://api.powermall.com.bd/orders/${_id}`, { status: "Processing" })
            .then(res => {

                if (res.data.modifiedCount > 0) {

                    axios.get(`https://api.powermall.com.bd/orders/${_id}`)
                        .then(res => {
                            if (res.status === 200) {

                                const statusdata = res.data;

                                axios.post('https://api.powermall.com.bd/orderstatus', statusdata)
                                    .then(res => {
                                        if (res.status === 200) {
                                            setRerender(rerender + 1);
                                            Swal.fire(
                                                'Successful',
                                                'Order Has been Sent To Order Status Successfully',
                                                'success'
                                            );
                                        }
                                    });

                            }

                        });

                }

            });

    };

    const handleSearch = (e) => {

        const searchText = e.target.value;
        const matchedOrders = pd.filter(p => p?.email.toLowerCase().includes(searchText.toLowerCase()));
        setOrders(matchedOrders);

    };

    return (
        <div>
            <div className='grid grid-cols-1 md:grid-cols-2 px-5 py-8 border border-gray-200 rounded-lg items-center shadow-lg mb-10'>

                <h1 className='text-xl font-medium mb-2 md:mb-0'>Orders</h1>
                <div className='relative'>
                    <BsSearch className='absolute top-4 left-3 text-gray-600' />
                    <input onChange={handleSearch} className='px-10 py-3 border border-gray-200 rounded-lg outline-none focus:ring-2 w-full ring-blue-500 transition duration-300' type="text" placeholder='Search Orders By Users Email' />
                </div>
            </div>




            <div className="flex flex-col shadow-2xl">
                <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-6">
                    <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-6">
                        <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table className="min-w-full divide-y divide-gray-200">
                                <thead className="bg-gray-50">

                                    <tr>

                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Order ID
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Name & Email
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Address
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Date
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Time
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Quantity
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Price
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Status
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Delivery Method
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Products
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Invoice
                                        </th>

                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                            Actions
                                        </th>

                                    </tr >

                                </thead>

                                <tbody className="bg-white divide-y divide-gray-200">

                                    {orders.length === 0 ? <tr>

                                        <td colSpan={12} className='py-3'>

                                            <h1 className='text-gray-400 text-sm font-medium text-center'>No Data Found</h1>

                                        </td>

                                    </tr> : ''}

                                    {orders?.map((item) => {

                                        return (
                                            <tr key={item._id} className='hover:bg-gray-100'>

                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <h1 className="px-2 text-sm leading-5 flex justify-center">
                                                        {item?.orderID}
                                                    </h1>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <h1 className="px-2 text-sm leading-5 flex text-gray-600 font-bold justify-center">
                                                        {item?.name}
                                                    </h1>
                                                    <h1 className="px-2 text-xs leading-5 flex justify-center">
                                                        {item?.email}
                                                    </h1>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <h1 onClick={() => handleAddress(item?._id)} title={item?.address} className="px-2 text-sm leading-5 truncate w-20 mx-auto cursor-pointer">
                                                        {item?.address}
                                                    </h1>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <h1 className="px-2 text-sm leading-5 flex justify-center">
                                                        {item?.date}
                                                    </h1>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <h1 className="px-2 text-sm leading-5 flex justify-center">
                                                        {item?.time}
                                                    </h1>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <h1 className="px-2 text-sm leading-5 flex justify-center">
                                                        {item?.cart?.length}
                                                    </h1>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <h1 className="px-2 text-sm font-semibold leading-5 flex justify-center">
                                                        &#2547; {item?.price?.toFixed(0)}
                                                    </h1>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">

                                                    <h1 className={`p-1 w-20 text-center mx-auto rounded-full text-xs ${item?.status === "Pending" ? " bg-red-200 text-red-900" : item?.status === "Delivered" ? "bg-green-200 text-green-900" : "bg-yellow-200 text-yellow-900"}`}>
                                                        {item?.status}
                                                    </h1>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">

                                                    <h1 className=" text-xs">
                                                        {item?.deliveryMethod}
                                                    </h1>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <h1 className="border w-20 py-1 bg-blue-500 text-white mx-auto text-sm  rounded-full leading-5 flex justify-center cursor-pointer" onClick={() => handleModal(item?._id)}>
                                                        View
                                                    </h1>
                                                </td>

                                                {/* onClick={() => navigate(`/profile/invoice/${item?._id}`)} */}
                                                <td className="px-3 py-4 whitespace-nowrap">
                                                    <span className="px-3 bg-blue-600 rounded-md py-1 text-xs leading-5 font-semibold text-white flex justify-center  hover:bg-blue-700 cursor-pointer" onClick={() => navigate(`/dashboard/orders/${item?._id}`)} >
                                                        Invoice
                                                    </span>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">

                                                    <div className='flex gap-x-5 justify-center'>

                                                        <button onClick={() => handleDelete(item?._id)} className='p-2 border rounded-full bg-red-500  hover:bg-red-700 text-white transition duration-500 shadow-lg'>
                                                            <AiOutlineDelete className='text-xl' />
                                                        </button>

                                                        <button onClick={() => sendToOrderStatus(item?._id)} className='p-2 border rounded-full bg-green-500 hover:bg-green-700 text-white transition duration-500 shadow-lg'><BsCheck2 className='text-xl' /></button>

                                                    </div>

                                                </td>
                                                <ProductModal open={open} setOpen={setOpen} product={product} id={item?._id} />
                                                {/* <InvoiceTemplate open={open} setOpen={setOpen} order={order} id={item?._id} /> */}
                                                <AddressModal addressOpen={addressOpen} setAddressOpen={setAddressOpen} address={address1} id={item?._id} />

                                            </tr>
                                        );
                                    })}
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div >
    );
};

export default Orders;
