import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiOutlineDelete } from 'react-icons/ai';
import { BsPlus, BsSearch } from 'react-icons/bs';
import { FiEdit } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

const Coupons = () => {

    const [coupons, setCoupons] = useState([]);
    const [displayCoupons, setDisplayCoupons] = useState([]);

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/coupons')
            .then(res => {
                setCoupons(res.data);
                setDisplayCoupons(res.data);
            });

    }, []);

    const handleDelete = (_id) => {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                axios.delete(`https://api.powermall.com.bd/coupons/${_id}`)
                    .then(res => {
                        if (res.data.deletedCount > 0) {
                            Swal.fire(
                                'Deleted!',
                                'Category Has Been Deleted.',
                                'success'
                            );
                        }
                    });

            }
        });

    };

    const handleSearch = (e) => {

        const searchText = e.target.value;
        const machedCoupons = coupons?.filter(coupon => coupon?.code.toLowerCase().includes(searchText.toLowerCase()));
        setDisplayCoupons(machedCoupons);
    };

    return (
        <div>
            <div>

                <div className='grid grid-cols-1 md:grid-cols-2 px-5 py-8 border border-gray-200 rounded-lg items-center shadow-lg mb-10'>

                    <h1 className='font-medium text-xl mb-2 md:mb-2'>Coupons</h1>
                    <div className='relative flex flex-col lg:flex-row gap-4'>
                        <BsSearch className='absolute top-4 left-3 text-gray-600' />
                        <input onChange={handleSearch} className='px-10 py-3 border border-gray-200 rounded-lg outline-none focus:ring-2 w-full ring-blue-500 transition duration-300' type="text" placeholder='Search Coupons By There Name' />
                        <Link to='/dashboard/addcoupons'>
                            <button className='flex items-center gap-x-2 px-5 py-3 rounded-lg bg-blue-500 hover:bg-blue-600 transition duration-500 text-whitering-offset-2 text-white'><BsPlus className='text-2xl' />Coupons</button>
                        </Link>
                    </div>

                </div>


                <div className="flex flex-col shadow-2xl">
                    <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-6">
                        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-6">
                            <div className="overflow-hidden border-b border-gray-200 sm:rounded-lg shadow">
                                <table className="min-w-full divide-y divide-gray-200">
                                    <thead className="bg-gray-50">
                                        <tr>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                ID
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Code
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Amount
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Active
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Expired
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Actions
                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody className="bg-white divide-y divide-gray-200 text-gray-600">
                                        {displayCoupons.length === 0 ? <tr>

                                            <td colSpan={12} className='py-3'>

                                                <h1 className='text-gray-400 text-sm font-medium text-center'>No Data Found</h1>

                                            </td>

                                        </tr> : ''}
                                        {
                                            displayCoupons.map((coupon, index) => (

                                                <tr key={coupon._id} className='hover:bg-gray-100 text-center'>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{index}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{coupon?.code}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>&#2547; {coupon?.ammount}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{coupon?.start}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{coupon?.end}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap text-sm text-gray-500">
                                                        <div className='flex justify-center gap-x-4'>
                                                            <button onClick={() => handleDelete(coupon?._id)} className='p-2 border rounded-full bg-red-500  hover:bg-red-700 text-white transition duration-500 shadow-lg'><AiOutlineDelete className='text-xl' /></button>
                                                            <Link to={`/dashboard/updatecoupons/${coupon?._id}`}>
                                                                <button className='p-2 border rounded-full hover:bg-gray-200'><FiEdit className='text-xl' /></button>
                                                            </Link>
                                                        </div>
                                                    </td>

                                                </tr>

                                            ))
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Coupons;
