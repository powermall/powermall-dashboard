import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiOutlineDollarCircle } from 'react-icons/ai';
import { BsCurrencyDollar } from 'react-icons/bs';
import { MdAddShoppingCart, MdStorefront } from 'react-icons/md';
import { CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis } from "recharts";

const Sales = () => {

    const [orders, setOrders] = useState([]);

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/orders')
            .then(res => {
                setOrders(res.data);
            });
    }, []);

    const counts = [

        {
            title: 'Total Revenue',
            icons: <BsCurrencyDollar className='text-2xl text-green-600' />,
            price: '৳ 0.00'
        },
        {
            title: 'Total Order',
            icons: <MdAddShoppingCart className='text-2xl text-red-600' />,
            price: orders.length
        },
        {
            title: 'Todays Revenue',
            icons: <AiOutlineDollarCircle className='text-2xl text-yellow-600' />,
            price: '৳ 0.00'
        },
        {
            title: 'Total Sales',
            icons: <MdStorefront className='text-2xl text-blue-600' />,
            price: 80
        },

    ];

    const data = [

        {
            name: "Total Revenue",
            totalRevenue: 4000,
            totalOrders: 2400,
            todaysRevenue: 3600,
            totalSales: 3500
        },
        {
            name: "Total Order",
            totalRevenue: 3000,
            totalOrders: 2908,
            todaysRevenue: 4800,
            totalSales: 4500
        },
        {
            name: "Todays Revenue",
            totalRevenue: 2000,
            totalOrders: 9800,
            todaysRevenue: 6030,
            totalSales: 1800
        },
        {
            name: "Total Sales",
            totalRevenue: 2780,
            totalOrders: 3908,
            todaysRevenue: 6500,
            totalSales: 6600
        },

    ];

    let width;
    let height;
    if (window.innerWidth < 1025) {
        width = 300;
        height = 200;
    } else if (window.innerWidth > 1025) {
        width = 900;
        height = 500;
    }

    return (
        <div>

            <div>

                <div className='grid grid-cols-1 lg:grid-cols-4 gap-6'>

                    {
                        counts?.map((count, index) => (

                            <div key={index} className='border rounded-xl shadow-xl p-6 flex flex-col gap-y-6'>

                                <div className='flex items-center justify-between'>

                                    <div>
                                        <h1 className='font-bold'>{count?.title}</h1>
                                        <span className='text-sm'>(Last 30 Days)</span>
                                    </div>

                                    <div className='p-3 rounded-full border'>
                                        {count?.icons}
                                    </div>

                                </div>

                                <div>
                                    <h1 className='text-xl font-semibold'>{count?.price}</h1>
                                </div>

                            </div>

                        ))
                    }

                </div>

                <div className='mt-20'>

                    <div className='w-5/6 lg:w-3/5 mx-auto'>

                        <LineChart
                            width={width}
                            height={height}
                            data={data}
                        >
                            <CartesianGrid strokeDasharray="5 5" />
                            <XAxis dataKey="name" />
                            <YAxis />
                            <Tooltip />
                            <Legend style={{ paddingTop: '1rem' }} />
                            <Line
                                type="monotone"
                                dataKey="totalOrders"
                                stroke="#DC2626"
                                activeDot={{ r: 8 }}
                            />
                            <Line
                                type="monotone"
                                dataKey="todaysRevenue"
                                stroke="#059669"
                                activeDot={{ r: 8 }}
                            />
                            <Line
                                type="monotone"
                                dataKey="totalRevenue"
                                stroke="#D97706"
                                activeDot={{ r: 8 }}
                            />
                            <Line
                                type="monotone"
                                dataKey="totalSales"
                                stroke="#2563EB"
                                activeDot={{ r: 8 }}
                            />

                        </LineChart>

                    </div>

                </div>

            </div>

        </div>
    );
};

export default Sales;
