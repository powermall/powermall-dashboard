import React, { useEffect, useState } from 'react';
import { BsSearch } from 'react-icons/bs';

function Wishist(props) {
    const [products, setProducts] = useState([]);
    const [displayProducts, setDisplayProducts] = useState([]);

    useEffect(() => {

        fetch('https://api.powermall.com.bd/products')
            .then(res => res.json())
            .then(data => {
                setProducts(data);
                setDisplayProducts(data);
            });

    }, []);


    const handleSearch = (e) => {

        const searchText = e.target.value;
        const machedProducts = products?.filter(product => product?.name.toLowerCase().includes(searchText.toLowerCase()));
        setDisplayProducts(machedProducts);
    };

    return (
        <div>

            <div className='container text-sm'>

                <div className='grid grid-cols-1 md:grid-cols-2 px-5 py-8 border border-gray-200 rounded-lg items-center shadow-lg mb-10'>

                    <h1 className='text-xl font-medium mb-2 md:mb-0'>Upcoming Products</h1>
                    <div className='relative flex flex-col lg:flex-row gap-4'>
                        <BsSearch className='absolute top-4 left-3 text-gray-600' />
                        <input onChange={handleSearch} className='px-10 py-3 border border-gray-200 rounded-lg outline-none focus:ring-2 w-full ring-blue-500 transition duration-300' type="text" placeholder='Search Products By There Name' />
                      
                    </div>
                </div>

                <div className="flex flex-col shadow-2xl">
                    <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table className="min-w-full divide-y divide-gray-200">
                                    <thead className="bg-gray-50">
                                        <tr >
                                            <th
                                                scope="col"
                                                className="px-3 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Image
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Name
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Brand
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Category
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Wishlist Number
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody className="bg-white divide-y divide-gray-200">

                                        {displayProducts.length === 0 ? <tr>

                                            <td colSpan={12} className='py-3'>

                                                <h1 className='text-gray-400 text-sm font-medium text-center'>No Data Found</h1>

                                            </td>

                                        </tr> : ''}

                                        {
                                            displayProducts?.filter(item => item?.delivery === 'Upcoming')?.map(product => {
                                                const img = product.img.split(',');

                                                return (

                                                    <tr key={product._id} className='text-center hover:bg-gray-100'>

                                                        <td className="px-6 py-1 whitespace-nowrap">
                                                            <img className="w-10 rounded" src={img[0]} alt="" />
                                                        </td>

                                                        <td className="px-6 py-1 whitespace-nowrap">
                                                            <h1 title={product?.name} className='truncate w-48 mx-auto'>{product?.name}</h1>
                                                        </td>

                                                        <td className="px-6 py-1 whitespace-nowrap">
                                                            <h1>{product?.brand}</h1>
                                                        </td>

                                                        <td className="px-6 py-1 whitespace-nowrap">
                                                            <h1>{product?.category}</h1>
                                                        </td>
                                                        <td className="px-6 py-1 whitespace-nowrap">
                                                            <h1>100</h1>
                                                        </td>

                                                    </tr>

                                                );

                                            })

                                        }

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div >
    );
}

export default Wishist;