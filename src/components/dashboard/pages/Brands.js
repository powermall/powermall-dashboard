import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiOutlineDelete } from 'react-icons/ai';
import { BsPlus, BsSearch } from 'react-icons/bs';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

const Brands = () => {

    const [brands, setBrands] = useState([]);
    const [displayBrands, setDisplayBrands] = useState([]);

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/brands')
            .then(res => {
                setBrands(res.data);
                setDisplayBrands(res.data);
            });

    }, []);

    const handleDelete = (_id) => {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                axios.delete(`https://api.powermall.com.bd/brands/${_id}`)
                    .then(res => {
                        if (res.data.deletedCount > 0) {
                            Swal.fire(
                                'Deleted!',
                                'Brand Has Been Deleted.',
                                'success'
                            );
                        }
                    });
            }
        });

    };

    const handleSearch = (e) => {

        const searchText = e.target.value;
        const matcedBrands = brands.filter(brand => brand?.brand.toLowerCase().includes(searchText.toLowerCase()));
        setDisplayBrands(matcedBrands);

    };

    return (
        <div>

            <div className='container'>

                <div className='grid grid-cols-1 md:grid-cols-2 px-5 py-8 border border-gray-200 rounded-lg items-center shadow-lg mb-10'>

                    <h1 className='text-xl font-medium mb-2 md:mb-0'>Brands</h1>
                    <div className='relative flex flex-col lg:flex-row gap-4'>
                        <BsSearch className='absolute top-4 left-3 text-gray-600' />
                        <input onChange={handleSearch} className='px-10 py-3 border border-gray-200 rounded-lg outline-none focus:ring-2 w-full ring-blue-500 transition duration-300' type="text" placeholder='Search Brands By There Name' />
                        <Link to='/dashboard/addbrands'>
                            <button className='flex items-center gap-x-2 px-5 py-3 rounded-lg bg-blue-500 hover:bg-blue-600 transition duration-500 text-whitering-offset-2 text-white'><BsPlus className='text-2xl' />Brands</button>
                        </Link>
                    </div>
                </div>

                <div className="flex flex-col shadow-2xl">
                    <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table className="min-w-full divide-y divide-gray-200">
                                    <thead className="bg-gray-50">
                                        <tr >
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                ID
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Image
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Name
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Actions
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody className="bg-white divide-y divide-gray-200">

                                        {displayBrands.length === 0 ? <tr>

                                            <td colSpan={12} className='py-3'>

                                                <h1 className='text-gray-400 text-sm font-medium text-center'>No Data Found</h1>

                                            </td>

                                        </tr> : ''}

                                        {displayBrands.map((brand, index) => (

                                            <tr key={brand?._id} className='text-left hover:bg-gray-100'>

                                                <td className="px-6 py-2 whitespace-nowrap">
                                                    <h1>{index + 1}</h1>
                                                </td>

                                                <td className="px-6 py-2 whitespace-nowrap">
                                                    <img className='w-12' src={brand?.img} alt="brand" />
                                                </td>

                                                <td className="px-6 text-center py-2 whitespace-nowrap">
                                                    <h1>{brand?.brand}</h1>
                                                </td>

                                                <td className="px-6 py-2 whitespace-nowrap">
                                                    <div className='flex gap-x-4 justify-end'>

                                                        <button onClick={() => handleDelete(brand?._id)} className='p-2 border rounded-full bg-red-500  hover:bg-yellow-700 text-white transition duration-500 shadow-lg'><AiOutlineDelete className='text-xl' /></button>

                                                    </div>
                                                </td>

                                            </tr>

                                        ))}

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    );
};

export default Brands;
