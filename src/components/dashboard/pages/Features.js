import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiOutlineDelete } from 'react-icons/ai';
import { BsPlus, BsSearch } from 'react-icons/bs';
import { FiEdit } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

const Features = () => {

    const [features, setFeatures] = useState([]);

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/feature')
            .then(res => setFeatures(res.data));

    }, [features]);

    const handleDelete = (_id) => {


        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                axios.delete(`https://api.powermall.com.bd/feature/${_id}`)
                    .then(res => {
                        if (res.data.deletedCount > 0) {

                            Swal.fire(
                                'Deleted!',
                                'Feature successfully deleted.',
                                'success'
                            );
                        }
                    });

            }
        });


    };

    return (
        <div>

            <div>

                <div className='grid grid-cols-1 md:grid-cols-2 px-5 py-8 border border-gray-200 rounded-lg items-center shadow-lg mb-10'>

                    <h1 className='font-medium text-xl mb-2 md:mb-2'>Features</h1>
                    <div className='relative flex flex-col lg:flex-row gap-4'>
                        <BsSearch className='absolute top-4 left-3 text-gray-600' />
                        <input className='px-10 py-3 border border-gray-200 rounded-lg outline-none focus:ring-2 w-full ring-blue-500 transition duration-300' type="text" placeholder='Type your query and press enter' />
                        <Link to='/dashboard/addfeatures'>
                            <button className='flex items-center gap-x-2 px-5 py-3 rounded-lg bg-blue-500 hover:bg-blue-600 transition duration-500 text-whitering-offset-2 text-white'><BsPlus className='text-2xl' />Features</button>
                        </Link>
                    </div>

                </div>


                <div className="flex flex-col shadow-2xl">
                    <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-6">
                        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-6">
                            <div className="overflow-hidden border-b border-gray-200 sm:rounded-lg shadow">
                                <table className="min-w-full divide-y divide-gray-200">
                                    <thead className="bg-gray-50">
                                        <tr>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                ID
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Image
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Title
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Details
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Button Name
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Tag
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Actions
                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody className="bg-white divide-y divide-gray-200 text-gray-600">

                                        {features.length === 0 ? <tr>

                                            <td colSpan={12} className='py-3'>

                                                <h1 className='text-gray-400 text-sm font-medium text-center'>No Data Found</h1>

                                            </td>

                                        </tr> : ''}

                                        {
                                            features?.map((feature, index) => (

                                                <tr key={feature._id} className='hover:bg-gray-100 text-center'>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{index}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <img className='w-14 mx-auto' src={feature?.img} alt="feature" />
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{feature?.title}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{feature?.details}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{feature?.name}</h1>
                                                    </td>

                                                    <td title={feature?.tag} className="px-6 py-2 whitespace-nowrap">
                                                        <h1 className='w-60 text-sm truncate mx-auto hover:underline'>
                                                            {feature?.tag}
                                                        </h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap text-sm text-gray-500">
                                                        <div className='flex gap-x-4 justify-center'>

                                                            <button onClick={() => handleDelete(feature?._id)} className='p-2 border rounded-full bg-red-500  hover:bg-red-700 text-white transition duration-500 shadow-lg'><AiOutlineDelete className='text-xl' /></button>
                                                            <Link to={`/dashboard/updatefeatures/${feature?._id}`}>
                                                                <button className='p-2 border rounded-full hover:bg-gray-200'><FiEdit className='text-xl' /></button>
                                                            </Link>

                                                        </div>
                                                    </td>

                                                </tr>

                                            ))
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Features;
