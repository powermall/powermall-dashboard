import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiOutlineDelete } from 'react-icons/ai';
import { BsPlus, BsSearch } from 'react-icons/bs';
import { FiEdit } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

const Category = () => {

    const [categories, setCategories] = useState([]);
    const [displayCategories, setDisplayCategories] = useState([]);

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/category')
            .then(res => {
                const newData = res.data.sort((a, b) => a?.position - b?.position);
                setCategories(newData);
                setDisplayCategories(newData);
            });

    }, []);

    const handleSearch = (e) => {

        const searchText = e.target.value;
        const machedCategories = categories?.filter(category => category?.category.toLowerCase().includes(searchText.toLowerCase()));
        setDisplayCategories(machedCategories);

    };

    const handleDelete = (_id) => {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                axios.delete(`https://api.powermall.com.bd/category/${_id}`)
                    .then(res => {
                        if (res.data.deletedCount > 0) {

                            const newCategory = displayCategories.filter(displayCategory => displayCategory?._id !== _id);
                            setDisplayCategories(newCategory);
                            setCategories(newCategory);

                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                        }
                    });
            }
        });

    };

    return (
        <div>

            <div className='container'>

                <div className='grid grid-cols-1 md:grid-cols-2 px-5 py-8 border border-gray-200 rounded-lg items-center shadow-lg mb-10'>

                    <h1 className='text-xl font-medium mb-2 md:mb-0'>Category</h1>
                    <div className='relative flex flex-col lg:flex-row gap-4'>
                        <BsSearch className='absolute top-4 left-3 text-gray-600' />
                        <input onChange={handleSearch} className='px-10 py-3 border border-gray-200 rounded-lg outline-none focus:ring-2 w-full ring-blue-500 transition duration-300' type="text" placeholder='Search Category By There Name' />
                        <Link to='/dashboard/addcategory'>
                            <button className='flex items-center gap-x-2 px-5 py-3 rounded-lg bg-blue-500 hover:bg-blue-600 transition duration-500 text-whitering-offset-2 text-white'><BsPlus className='text-2xl' />Category</button>
                        </Link>
                    </div>
                </div>

                <div className="flex flex-col shadow-2xl">
                    <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table className="min-w-full divide-y divide-gray-200">
                                    <thead className="bg-gray-50">
                                        <tr >
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Position
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Image
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Banner
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Name
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Slug
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Sub Category
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Actions
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody className="bg-white divide-y divide-gray-200">

                                        {displayCategories.length === 0 ? <tr>

                                            <td colSpan={12} className='py-3'>

                                                <h1 className='text-gray-400 text-sm font-medium text-center'>No Data Found</h1>

                                            </td>

                                        </tr> : ''}

                                        {displayCategories.map((category) => (

                                            <tr key={category?._id} className='text-center hover:bg-gray-100'>

                                                <td className="px-6 text-center py-2 whitespace-nowrap">
                                                    <h1>{category?.position}</h1>
                                                </td>

                                                <td className="px-6 text-center py-2 whitespace-nowrap">
                                                    <img className='w-12 mx-auto' src={category?.img} alt="category" />
                                                </td>

                                                <td className="px-6 text-center py-2 whitespace-nowrap">
                                                    <img className='w-12 mx-auto' src={category?.banner} alt="category" />
                                                </td>

                                                <td className="px-6 py-2 whitespace-nowrap">
                                                    <h1>{category?.category}</h1>
                                                </td>

                                                <td className="px-6 py-2 whitespace-nowrap">
                                                    <h1>{category?.slug}</h1>
                                                </td>

                                                <td className="px-6 py-2 whitespace-nowrap">
                                                    <h1 className='capitalize text-sm flex gap-x-2 justify-center'>{category?.subCategory?.length !== 0 ? category?.subCategory?.join(' , ') : <h1 className='text-5xl font-thin'>-</h1>}</h1>
                                                </td>

                                                <td className="px-6 py-2 whitespace-nowrap">
                                                    <div className='flex gap-x-4 justify-center'>

                                                        <button onClick={() => handleDelete(category?._id)} className='p-2 border rounded-full bg-red-500  hover:bg-yellow-700 text-white transition duration-500 shadow-lg'><AiOutlineDelete className='text-xl' /></button>

                                                        <Link to={`/dashboard/updatecategory/${category?._id}`}>
                                                            <button className='p-2 border rounded-full hover:bg-gray-200'><FiEdit className='text-xl' /></button>
                                                        </Link>

                                                    </div>
                                                </td>

                                            </tr>

                                        ))}

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    );
};

export default Category;
