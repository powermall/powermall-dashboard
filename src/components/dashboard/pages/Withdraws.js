import React from 'react';
import { BsPlus } from 'react-icons/bs';
import { FcCancel } from 'react-icons/fc';

const Withdraws = () => {

    const withdraws = [
        {
            name: 'Jane Cooper',
            img: 'https://admin-pickbazar.vercel.app/_next/image?url=%2Favatar-placeholder.svg&w=48&q=75',
            code: 'hello99',
            ammount: '10',
            status: '1',
            country: 'Bangladesh',
            city: 'Dhaka',
            state: 'x',
            zip: 2065,
            goal: 'true',
            type: 'fixed'
        },
    ];

    return (
        <div>

            <div className='grid grid-cols-1 md:grid-cols-2 px-5 py-8 border border-gray-200 rounded-lg items-center shadow-lg mb-10'>

                <h1>Withdraws</h1>

            </div>

            <div>
                <div className="flex flex-col shadow-2xl">
                    <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-6">
                        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-6">
                            <div className="overflow-hidden border-b border-gray-200 sm:rounded-lg shadow">
                                <table className="min-w-full divide-y divide-gray-200">
                                    <thead className="bg-gray-50">
                                        <tr>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Shop Name
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Amount
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Status
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Name
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            >
                                                Actions
                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody className="bg-white divide-y divide-gray-200 text-gray-600">
                                        {
                                            withdraws?.map((ship, index) => (

                                                <tr key={index} className='hover:bg-gray-100 text-center'>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{ship.state}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{ship?.state}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{ship?.state}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap">
                                                        <h1>{ship?.state}</h1>
                                                    </td>

                                                    <td className="px-6 py-2 whitespace-nowrap text-sm text-gray-500">
                                                        <div className='flex justify-center gap-x-2'>
                                                            <button className='p-2 border rounded-full hover:bg-gray-200 hover:text-green-600'><BsPlus className='text-xl' /></button>
                                                            <button className='p-2 border rounded-full hover:bg-gray-200'><FcCancel className='text-xl' /></button>
                                                        </div>
                                                    </td>

                                                </tr>

                                            ))
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Withdraws;
