import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import Swal from 'sweetalert2';

function Notice(props) {
    
    const [notice, setNotice] = useState([]);
    const [disabled, setDisabled] = useState(false);
    const messageRef = useRef('');
    const statusRef = useRef('');

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/notice')
            .then(res => setNotice(res?.data?.[0]));

    }, []);

    const handleStatusUpdate = (e) => {
        e.preventDefault();

        const notice = messageRef.current.value;
        const status = statusRef.current.value;
        const data = { notice, status };

        axios.put('https://api.powermall.com.bd/notice/621df6ad30eb18a638851867', data)
            .then(res => {
                if (res.data.modifiedCount > 0) {
                    Swal.fire(
                        'Successful',
                        'Notice has Changed Successfully',
                        'success'
                    );
                    setDisabled(false);
                }
            });

    };

    const handleStatusChange = (e) => {

        const updatedStatus = e.target.value;
        const updatedNotification = { ...notice };
        updatedNotification.status = updatedStatus;
        setNotice(updatedNotification);

    };


    return (
        <div className='mb-20 lg:mb-0'>
            <div className="mt-10 sm:mt-0">
                <div className="md:grid md:grid-cols-3 md:gap-6">
                    <div className="md:col-span-1">
                        <div className="px-4 sm:px-0">
                            <h3 className="text-xl font-medium leading-6 text-gray-900">Notice</h3>
                            <div className='w-14 h-1 rounded-full bg-blue-500 my-3'></div>
                            <p className="mt-1 text-sm text-gray-600">Change your site notice from here</p>
                        </div>
                    </div>
                    <div className="mt-5 md:mt-0 md:col-span-2">
                        <form onSubmit={handleStatusUpdate} onChange={() => setDisabled(true)}>
                            <div className="shadow-lg overflow-hidden sm:rounded-md">
                                <div className="px-4 py-5 bg-white sm:p-6">
                                    <div className="grid grid-cols-6 gap-6">
                                        <div className="col-span-6 sm:col-span-5">
                                            <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                Notice Message
                                            </label>
                                            <input
                                                required
                                                ref={messageRef}
                                                defaultValue={notice?.notice}
                                                type="text"
                                                name="first-name"
                                                id="first-name"
                                                className="mt-1 px-5 py-3 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                        <div className="col-span-6 sm:col-span-1">
                                            <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                                                Notice
                                            </label>
                                            <select
                                                ref={statusRef}
                                                value={notice?.status}
                                                onChange={handleStatusChange}
                                                className="mt-1 block w-full py-3 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm">
                                                <option>On</option>
                                                <option>Off</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6 mt-5">
                                    {

                                        disabled ? (

                                            <button
                                                onClick={handleStatusUpdate}
                                                type="submit"
                                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                            >
                                                Save
                                            </button>

                                        ) : (

                                            <button
                                                disabled
                                                type="submit"
                                                className="opacity-20 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 cursor-not-allowed"
                                            >
                                                Save
                                            </button>

                                        )

                                    }
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Notice;