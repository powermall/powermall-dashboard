import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import Swal from 'sweetalert2';

const Copyright = () => {
    const [copyrightText, setCopyrightText] = useState({});
    const [disabled, setDisabled] = useState(false);
    const text = useRef('');

    useEffect(() => {

        axios.get('https://api.powermall.com.bd/copyright/61dc1d87e7096f4da08ec024')
            .then(res => setCopyrightText(res.data));

    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();

        const copyrighttext = text.current.value;

        const data = { copyrighttext };
        axios.put('https://api.powermall.com.bd/copyright/61dc1d87e7096f4da08ec024', data)
            .then(res => {
                if (res.data.modifiedCount > 0) {

                    Swal.fire(
                        'Successful',
                        'Copyright Changed Successfully',
                        'success'
                    );
                    setDisabled(false);
                }
            });

    };

    return (
        <div>
            <div className="mt-10 sm:mt-0">
                <div className="md:grid md:grid-cols-3 md:gap-6">
                    <div className="md:col-span-1">
                        <div className="px-4 sm:px-0">
                            <h3 className="text-xl font-medium leading-6 text-gray-900">Copy Right Text</h3>
                            <div className='w-16 h-1 rounded-full bg-blue-500 my-3'></div>
                            <p className="mt-1 text-sm text-gray-600">Change your site information from here</p>
                        </div>
                    </div>
                    <div className="mt-5 md:mt-0 md:col-span-2">
                        <form onSubmit={handleSubmit}>
                            <div className="shadow-lg overflow-hidden sm:rounded-md">
                                <div className="px-4 py-5 bg-white sm:p-6">
                                    <div className="grid grid-cols-6 gap-6">

                                        <div className="col-span-6 sm:col-span-3 lg:col-span-6">
                                            <label htmlFor="region" className="block text-sm font-medium text-gray-700">
                                                Text
                                            </label>
                                            <input
                                                ref={text}
                                                onChange={() => setDisabled(true)}
                                                defaultValue={copyrightText?.copyrighttext}
                                                type="text"
                                                name="region"
                                                id="region"
                                                className="mt-1 px-5 py-2 border border-gray-300 rounded-lg w-full outline-none focus:ring-2 ring-blue-500 transition duration-300"
                                            />
                                        </div>

                                    </div>
                                </div>
                                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    {

                                        disabled ? (

                                            <button
                                                onClick={handleSubmit}
                                                type="submit"
                                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                            >
                                                Save
                                            </button>

                                        ) : (

                                            <button
                                                disabled
                                                type="submit"
                                                className="opacity-20 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 cursor-not-allowed"
                                            >
                                                Save
                                            </button>

                                        )

                                    }
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Copyright;
